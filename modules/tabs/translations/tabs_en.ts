<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="en_US">
<context>
    <name>@default</name>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="4"/>
        <location filename="../.configuration-ui-translations.cpp" line="54"/>
        <source>Chat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="7"/>
        <location filename="../.configuration-ui-translations.cpp" line="57"/>
        <source>Tabs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="10"/>
        <source>General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="13"/>
        <source>Use tabs by default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="16"/>
        <source>Minimum number of tabs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="18"/>
        <source>Always use tabs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="21"/>
        <source>Tabs at the bottom of the window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="24"/>
        <source>Auto tab change</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="27"/>
        <source>Conferences in tabs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="30"/>
        <source>Closing tabs using button from titlebar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="32"/>
        <source>&lt;nobr&gt;&lt;b&gt;Warning: This option has know bug!&lt;/b&gt;&lt;/nobr&gt;&lt;br&gt;If there is open more than one tab then exiting from KDE will be stopped by this module.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="36"/>
        <source>Look</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="45"/>
        <source>Show Close button on each tab</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="51"/>
        <source>Shortcuts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="60"/>
        <source>Move tab left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="63"/>
        <source>Move tab right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="66"/>
        <source>Switch to previous tab</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="69"/>
        <source>Switch to next tab</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TabsManager</name>
    <message>
        <location filename="../tabs.cpp" line="87"/>
        <source>Chat in New Window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../tabs.cpp" line="89"/>
        <location filename="../tabs.cpp" line="134"/>
        <source>Chat in New Tab</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../tabs.cpp" line="141"/>
        <source>Attach Chat to Tabs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../tabs.cpp" line="457"/>
        <source>NEW MESSAGE(S)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../tabs.cpp" line="539"/>
        <source>Detach</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../tabs.cpp" line="540"/>
        <source>Detach all</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../tabs.cpp" line="542"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../tabs.cpp" line="543"/>
        <source>Close all</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../tabs.cpp" line="755"/>
        <source>Conference [%1]</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
