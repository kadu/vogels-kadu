<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="de">
<context>
    <name>@default</name>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="4"/>
        <location filename="../.configuration-ui-translations.cpp" line="54"/>
        <source>Chat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="7"/>
        <location filename="../.configuration-ui-translations.cpp" line="57"/>
        <source>Tabs</source>
        <translation>Reiter</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="10"/>
        <source>General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="13"/>
        <source>Use tabs by default</source>
        <translation>Benutze Reiter standardmäßig</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="16"/>
        <source>Minimum number of tabs</source>
        <translation>Maximale Anzahl an Reitern</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="18"/>
        <source>Always use tabs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="21"/>
        <source>Tabs at the bottom of the window</source>
        <translation>Reiter unter</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="24"/>
        <source>Auto tab change</source>
        <translation>Automatisch Reiter wechseln</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="27"/>
        <source>Conferences in tabs</source>
        <translation>Konferenzen in Reitern</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="30"/>
        <source>Closing tabs using button from titlebar</source>
        <translation>Reiter mit dem Knopf aus der Titelleiste schließen</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="32"/>
        <source>&lt;nobr&gt;&lt;b&gt;Warning: This option has know bug!&lt;/b&gt;&lt;/nobr&gt;&lt;br&gt;If there is open more than one tab then exiting from KDE will be stopped by this module.</source>
        <translation>&lt;nobr&gt;&lt;b&gt;Achtung: Diese Option hat einen Bug&lt;/b&gt;&lt;/nobr&gt;&lt;br&gt;Wenn mehr als ein Reiter offen ist, wird der Versuch, KDE zu verlassen durch dieses Modul unterbrochen.</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="36"/>
        <source>Look</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="45"/>
        <source>Show Close button on each tab</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="51"/>
        <source>Shortcuts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="60"/>
        <source>Move tab left</source>
        <translation>Verschiebe Reiter nach links</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="63"/>
        <source>Move tab right</source>
        <translation>Verschiebe Reiter nach rechts</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="66"/>
        <source>Switch to previous tab</source>
        <translation>Zum vorherigen Reiter umschalten</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="69"/>
        <source>Switch to next tab</source>
        <translation>Zum nächsten Reiter umschalten</translation>
    </message>
</context>
<context>
    <name>TabsManager</name>
    <message>
        <location filename="../tabs.cpp" line="87"/>
        <source>Chat in New Window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../tabs.cpp" line="89"/>
        <location filename="../tabs.cpp" line="134"/>
        <source>Chat in New Tab</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../tabs.cpp" line="141"/>
        <source>Attach Chat to Tabs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../tabs.cpp" line="457"/>
        <source>NEW MESSAGE(S)</source>
        <translation>-&gt;Neue Nachrichten&lt;-</translation>
    </message>
    <message>
        <location filename="../tabs.cpp" line="539"/>
        <source>Detach</source>
        <translation>Abtrennen</translation>
    </message>
    <message>
        <location filename="../tabs.cpp" line="540"/>
        <source>Detach all</source>
        <translation>Alle abtrennen</translation>
    </message>
    <message>
        <location filename="../tabs.cpp" line="542"/>
        <source>Close</source>
        <translation>Schließen</translation>
    </message>
    <message>
        <location filename="../tabs.cpp" line="543"/>
        <source>Close all</source>
        <translation>Alle schließen</translation>
    </message>
    <message>
        <location filename="../tabs.cpp" line="755"/>
        <source>Conference [%1]</source>
        <translation>Konferenz [%1]</translation>
    </message>
</context>
</TS>
