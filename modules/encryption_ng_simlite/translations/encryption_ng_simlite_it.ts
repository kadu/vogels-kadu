<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="it">
<context>
    <name>EncryptioNgSimliteEncryptor</name>
    <message>
        <location filename="../encryption-ng-simlite-encryptor.cpp" line="87"/>
        <source>Cannot use public key: not a valid RSA key</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../encryption-ng-simlite-encryptor.cpp" line="106"/>
        <source>Cannot use public key: invalid BASE64 encoding</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../encryption-ng-simlite-encryptor.cpp" line="117"/>
        <source>Cannot use public key: invalid PKCS1 certificate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../encryption-ng-simlite-encryptor.cpp" line="124"/>
        <source>Cannot use public key: this key does not allow encrypttion</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../encryption-ng-simlite-encryptor.cpp" line="136"/>
        <source>Cannot encrypt: valid public key not available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../encryption-ng-simlite-encryptor.cpp" line="147"/>
        <source>Cannot encrypt: valid blowfish key not available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../encryption-ng-simlite-encryptor.cpp" line="175"/>
        <location filename="../encryption-ng-simlite-encryptor.cpp" line="189"/>
        <source>Cannot encrypt: unknown error</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
