<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="pl">
<context>
    <name>@default</name>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="4"/>
        <source>Chat</source>
        <translation>Rozmowa</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="7"/>
        <source>History</source>
        <translation>Historia</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="10"/>
        <source>General</source>
        <translation>Ogólne</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="13"/>
        <source>Click Me !1!</source>
        <translation>Kliknij mnie !1!</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="17"/>
        <source>Database Connection Settings</source>
        <translation>Ustawienia połączenia do bazy danych</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="20"/>
        <source>Choose database type</source>
        <translation>Wybierz typ bazy danych</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="22"/>
        <source>Choose type of databse, in which data should be stored</source>
        <translation>Wybierz typ bazy danych do przechowywania danych</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="25"/>
        <source>SQLite</source>
        <translation>SQLite</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="28"/>
        <source>MySQL</source>
        <translation>MySQL</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="31"/>
        <source>PostgreSQL</source>
        <translation>PostgreSQL</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="35"/>
        <source>Database host</source>
        <translation>Host</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="38"/>
        <source>Port</source>
        <translation>Port</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="41"/>
        <source>Database name</source>
        <translation>Nazwa bazy danych</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="44"/>
        <source>User name</source>
        <translation>Nazwa użytkownika</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="47"/>
        <source>Password</source>
        <translation>Hasło</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="50"/>
        <source>Table name prefix</source>
        <translation>Prefix dla nazw tablic</translation>
    </message>
</context>
<context>
    <name>HistorySqlStorage</name>
    <message>
        <location filename="../storage/history-sql-storage.cpp" line="83"/>
        <location filename="../storage/history-sql-storage.cpp" line="106"/>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <location filename="../storage/history-sql-storage.cpp" line="84"/>
        <source>It seems your Qt library does not provide support for selected database.
 Please select another driver in configuration window or install Qt with %1 plugin.</source>
        <translation>Zainstalowana biblioteka Qt nie wspiera wybranego typu bazy danych.
Proszę wybrać inny typ bazy danych w oknie konfiguracji lub zainstalować bibliotekę Qt z pluginem %1.</translation>
    </message>
</context>
</TS>
