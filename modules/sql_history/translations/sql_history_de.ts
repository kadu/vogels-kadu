<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="de">
<context>
    <name>@default</name>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="4"/>
        <source>Chat</source>
        <translation>Unterhaltung</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="7"/>
        <source>History</source>
        <translation>Verlauf</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="10"/>
        <source>General</source>
        <translation>Allgemein</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="13"/>
        <source>Click Me !1!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="17"/>
        <source>Database Connection Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="20"/>
        <source>Choose database type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="22"/>
        <source>Choose type of databse, in which data should be stored</source>
        <translation>Wählen Sie den Typ der Datenbank an, in denen Daten gespeichert werden sollen</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="25"/>
        <source>SQLite</source>
        <translation>SQLite</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="28"/>
        <source>MySQL</source>
        <translation>MySQL</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="31"/>
        <source>PostgreSQL</source>
        <translation>PostgreSQL</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="35"/>
        <source>Database host</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="38"/>
        <source>Port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="41"/>
        <source>Database name</source>
        <translation>Datenbanknamen</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="44"/>
        <source>User name</source>
        <translation>Benutzername</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="47"/>
        <source>Password</source>
        <translation>Passwort</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="50"/>
        <source>Table name prefix</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HistorySqlStorage</name>
    <message>
        <location filename="../storage/history-sql-storage.cpp" line="83"/>
        <location filename="../storage/history-sql-storage.cpp" line="106"/>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <location filename="../storage/history-sql-storage.cpp" line="84"/>
        <source>It seems your Qt library does not provide support for selected database.
 Please select another driver in configuration window or install Qt with %1 plugin.</source>
        <translation>Es scheint, Ihr Qt-Library bietet keine Unterstützung für ausgewählte Datenbank.
Bitte wählen Sie einen anderen Treiber in Konfigurationsfenster oder installieren Sie Qt mit %1 Plugin.</translation>
    </message>
</context>
</TS>
