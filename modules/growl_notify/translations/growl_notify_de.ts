<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="de">
<context>
    <name>GrowlNotifyConfigurationWidget</name>
    <message>
        <location filename="../growl_notify_configuration_widget.cpp" line="38"/>
        <source>
%&amp;t - title (eg. New message) %&amp;m - notification text (eg. Message from Jim), %&amp;d - details (eg. message quotation),
%&amp;i - notification icon</source>
        <translation>%&amp;t - Titel (z.B neue Nachricht) %&amp;m - Inhalt der Benachrichtigung (z.B. Nachricht von neo), %&amp;d - Details (z.B. Nachrichteninhalt),%&amp;i - Smiley</translation>
    </message>
    <message>
        <location filename="../growl_notify_configuration_widget.cpp" line="53"/>
        <source>Title</source>
        <translation>Titel</translation>
    </message>
    <message>
        <location filename="../growl_notify_configuration_widget.cpp" line="55"/>
        <source>Syntax</source>
        <translation>Syntax</translation>
    </message>
</context>
</TS>
