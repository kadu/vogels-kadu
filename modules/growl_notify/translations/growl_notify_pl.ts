<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="pl">
<context>
    <name>GrowlNotifyConfigurationWidget</name>
    <message>
        <location filename="../growl_notify_configuration_widget.cpp" line="38"/>
        <source>
%&amp;t - title (eg. New message) %&amp;m - notification text (eg. Message from Jim), %&amp;d - details (eg. message quotation),
%&amp;i - notification icon</source>
        <translation>%&amp;t - tytuł (np. Nowa wiadomość) %&amp;m - treść powiadomienia (np. Wiadomość od Jasia), %&amp;d - szczegóły (np. treść wiadomości),%&amp;i - ikona powiadomienia</translation>
    </message>
    <message>
        <location filename="../growl_notify_configuration_widget.cpp" line="53"/>
        <source>Title</source>
        <translation>Tytuł</translation>
    </message>
    <message>
        <location filename="../growl_notify_configuration_widget.cpp" line="55"/>
        <source>Syntax</source>
        <translation>Treść</translation>
    </message>
</context>
</TS>
