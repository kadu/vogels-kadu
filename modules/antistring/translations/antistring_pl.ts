<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="pl">
<context>
    <name>@default</name>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="4"/>
        <source>Chat</source>
        <translation>Rozmowa</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="7"/>
        <source>Antistring</source>
        <translation>Antyłańcuszek</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="10"/>
        <source>General</source>
        <translation>Ogólne</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="13"/>
        <source>Enable Antistring</source>
        <translation>Włącz antyłańcuszek</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="16"/>
        <source>Block message</source>
        <translation>Blokuj wiadomość</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="19"/>
        <source>Admonition</source>
        <translation>Pouczenie</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="24"/>
        <location filename="../.configuration-ui-translations.cpp" line="30"/>
        <source>Log</source>
        <translation>Logowanie</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="27"/>
        <source>Write log to file</source>
        <translation>Loguj wiadomości do pliku</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="35"/>
        <source>Conditions</source>
        <translation>Warunki</translation>
    </message>
</context>
<context>
    <name>Antistring</name>
    <message>
        <location filename="../antistring.cpp" line="118"/>
        <source>     DATA AND TIME      ::   ID   ::    MESSAGE
</source>
        <translation>     DATA I CZAS      ::   ID   ::    WIADOMOŚĆ
</translation>
    </message>
</context>
<context>
    <name>AntistringConfigurationUiHandler</name>
    <message>
        <location filename="../antistring-configuration-ui-handler.cpp" line="80"/>
        <source>Condition</source>
        <translation>Warunek</translation>
    </message>
    <message>
        <location filename="../antistring-configuration-ui-handler.cpp" line="86"/>
        <source>Don&apos;t use</source>
        <translation>Nie używaj</translation>
    </message>
    <message>
        <location filename="../antistring-configuration-ui-handler.cpp" line="87"/>
        <source>Factor</source>
        <translation>Współczynnik</translation>
    </message>
    <message>
        <location filename="../antistring-configuration-ui-handler.cpp" line="90"/>
        <source>Add</source>
        <translation>Dodaj</translation>
    </message>
    <message>
        <location filename="../antistring-configuration-ui-handler.cpp" line="91"/>
        <source>Change</source>
        <translation>Zmień</translation>
    </message>
    <message>
        <location filename="../antistring-configuration-ui-handler.cpp" line="92"/>
        <source>Delete</source>
        <translation>Usuń</translation>
    </message>
</context>
<context>
    <name>AntistringNotification</name>
    <message>
        <location filename="../antistring-notification.cpp" line="43"/>
        <source>Antistring</source>
        <translation>Antyłańcuszek</translation>
    </message>
    <message>
        <location filename="../antistring-notification.cpp" line="44"/>
        <source>Your interlocutor send you love letter</source>
        <translation>Twój rozmówca wysłał Ci łańcuszek szczęścia</translation>
    </message>
</context>
</TS>
