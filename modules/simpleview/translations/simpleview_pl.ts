<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="pl">
<context>
    <name>@default</name>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="4"/>
        <source>Look</source>
        <translation>Wygląd</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="7"/>
        <source>Simple view</source>
        <translation>Widok uproszczony</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="10"/>
        <source>General</source>
        <translation>Ogólne</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="13"/>
        <source>Keep contact list size</source>
        <translation>Zachowuj rozmiar listy kontaktów</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="16"/>
        <source>Hide scroll bar</source>
        <translation>Ukryj pasek przewijania</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="19"/>
        <source>Borderless</source>
        <translation>Ukryj obramowanie</translation>
    </message>
</context>
<context>
    <name>SimpleView</name>
    <message>
        <location filename="../simpleview.cpp" line="50"/>
        <source>Simple view</source>
        <translation>Widok uproszczony</translation>
    </message>
</context>
</TS>
