<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="pl">
<context>
    <name>@default</name>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="4"/>
        <source>MediaPlayer</source>
        <translation>MediaPlayer</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="7"/>
        <source>General</source>
        <translation>Ogólne</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="10"/>
        <source>Media Player Daemon Settings</source>
        <translation>Ustawienia MPD</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="13"/>
        <source>Host</source>
        <translation>Host</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="15"/>
        <source>Media Player Daemon server address</source>
        <translation>Adres serwera MPD</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="18"/>
        <source>Port</source>
        <translation>Port</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="20"/>
        <source>Port number MPD is listening on</source>
        <translation>Port, na którym MPD nasłuchuje</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="23"/>
        <source>Connection timeout (seconds)</source>
        <translation>Przekroczenie czasu połączenia (w sekundach)</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="25"/>
        <source>Number of seconds before the connection is terminated</source>
        <translation>Ilość sekund, po których połączenie zostanie przerwane</translation>
    </message>
</context>
</TS>
