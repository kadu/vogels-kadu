<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="it">
<context>
    <name>@default</name>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="4"/>
        <source>MediaPlayer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="7"/>
        <source>General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="10"/>
        <source>Media Player Daemon Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="13"/>
        <source>Host</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="15"/>
        <source>Media Player Daemon server address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="18"/>
        <source>Port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="20"/>
        <source>Port number MPD is listening on</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="23"/>
        <source>Connection timeout (seconds)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="25"/>
        <source>Number of seconds before the connection is terminated</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
