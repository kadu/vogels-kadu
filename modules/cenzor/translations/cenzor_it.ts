<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="it">
<context>
    <name>@default</name>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="4"/>
        <source>Chat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="7"/>
        <source>Cenzor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="10"/>
        <source>General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="13"/>
        <source>Enable cenzor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="16"/>
        <source>Admonition</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="21"/>
        <source>Swearwords</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="25"/>
        <source>Exclusions</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CenzorNotification</name>
    <message>
        <location filename="../notify/cenzor-notification.cpp" line="50"/>
        <source>Message was cenzored</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../notify/cenzor-notification.cpp" line="52"/>
        <source>Your interlocutor used obscene word and became admonished</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ListEditWidget</name>
    <message>
        <location filename="../gui/widgets/list-edit-widget.cpp" line="48"/>
        <source>Add</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/list-edit-widget.cpp" line="49"/>
        <source>Change</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/list-edit-widget.cpp" line="50"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
