<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="pl">
<context>
    <name>@default</name>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="4"/>
        <source>Chat</source>
        <translation>Rozmowa</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="7"/>
        <source>Cenzor</source>
        <translation>Cenzor</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="10"/>
        <source>General</source>
        <translation>Ogólne</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="13"/>
        <source>Enable cenzor</source>
        <translation>Włącz cenzora</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="16"/>
        <source>Admonition</source>
        <translation>Pouczenie</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="21"/>
        <source>Swearwords</source>
        <translation>Przekleństwa</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="25"/>
        <source>Exclusions</source>
        <translation>Wyjątki</translation>
    </message>
</context>
<context>
    <name>CenzorNotification</name>
    <message>
        <location filename="../notify/cenzor-notification.cpp" line="50"/>
        <source>Message was cenzored</source>
        <translation>Wiadomość została ocenzurowana</translation>
    </message>
    <message>
        <location filename="../notify/cenzor-notification.cpp" line="52"/>
        <source>Your interlocutor used obscene word and became admonished</source>
        <translation>Twój rozmówca Przeklina i został upomniany</translation>
    </message>
</context>
<context>
    <name>ListEditWidget</name>
    <message>
        <location filename="../gui/widgets/list-edit-widget.cpp" line="48"/>
        <source>Add</source>
        <translation>Dodaj</translation>
    </message>
    <message>
        <location filename="../gui/widgets/list-edit-widget.cpp" line="49"/>
        <source>Change</source>
        <translation>Zmień</translation>
    </message>
    <message>
        <location filename="../gui/widgets/list-edit-widget.cpp" line="50"/>
        <source>Delete</source>
        <translation>Usuń</translation>
    </message>
</context>
</TS>
