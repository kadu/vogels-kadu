<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="pl">
<context>
    <name>Infos</name>
    <message>
        <location filename="../infos.cpp" line="121"/>
        <source>&amp;Show infos about buddies...</source>
        <translation>&amp;Pokaż informacje o znajomych...</translation>
    </message>
</context>
<context>
    <name>InfosDialog</name>
    <message>
        <location filename="../infos_dialog.cpp" line="44"/>
        <source>Buddies Information</source>
        <translation>Informacje o znajomych</translation>
    </message>
    <message>
        <location filename="../infos_dialog.cpp" line="57"/>
        <source>Buddy</source>
        <translation>Znajomy</translation>
    </message>
    <message>
        <location filename="../infos_dialog.cpp" line="58"/>
        <source>Protocol</source>
        <translation>Protokół</translation>
    </message>
    <message>
        <location filename="../infos_dialog.cpp" line="59"/>
        <source>UIN</source>
        <translation>Numer GG</translation>
    </message>
    <message>
        <location filename="../infos_dialog.cpp" line="60"/>
        <source>Nick</source>
        <translation>Pseudonim</translation>
    </message>
    <message>
        <location filename="../infos_dialog.cpp" line="61"/>
        <source>IP</source>
        <translation>Adres IP</translation>
    </message>
    <message>
        <location filename="../infos_dialog.cpp" line="62"/>
        <source>Domain name</source>
        <translation>Nazwa domeny</translation>
    </message>
    <message>
        <location filename="../infos_dialog.cpp" line="63"/>
        <source>Description</source>
        <translation>Opis</translation>
    </message>
    <message>
        <location filename="../infos_dialog.cpp" line="64"/>
        <source>State</source>
        <translation>Stan</translation>
    </message>
    <message>
        <location filename="../infos_dialog.cpp" line="65"/>
        <source>Last time seen on</source>
        <translation>Ostatnio widziany</translation>
    </message>
    <message>
        <location filename="../infos_dialog.cpp" line="100"/>
        <source>&amp;Close</source>
        <translation>&amp;Zamknij</translation>
    </message>
</context>
</TS>
