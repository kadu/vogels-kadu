<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="en_US">
<context>
    <name>@default</name>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="4"/>
        <location filename="../.configuration-ui-translations.cpp" line="55"/>
        <source>Chat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="7"/>
        <source>History</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="10"/>
        <source>Chats History</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="13"/>
        <location filename="../.configuration-ui-translations.cpp" line="15"/>
        <source>Save messages in history</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="18"/>
        <source>Save chats with anonymous users</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="22"/>
        <source>Status Changes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="25"/>
        <source>Save status changes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="27"/>
        <source>Save status changes in history</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="30"/>
        <source>Save status only with description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="34"/>
        <source>Quotation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="37"/>
        <source>Quote message in chat window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="40"/>
        <source>Don&apos;t quote messages older than</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="43"/>
        <source>.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="49"/>
        <source>Shortcuts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="52"/>
        <source>General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="58"/>
        <source>View history</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BuddyStatusDatesModel</name>
    <message>
        <location filename="../model/buddy-status-dates-model.cpp" line="62"/>
        <source>Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../model/buddy-status-dates-model.cpp" line="63"/>
        <source>Length</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ChatDatesModel</name>
    <message>
        <location filename="../model/chat-dates-model.cpp" line="65"/>
        <source>Chat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../model/chat-dates-model.cpp" line="66"/>
        <source>Title</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../model/chat-dates-model.cpp" line="67"/>
        <source>Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../model/chat-dates-model.cpp" line="68"/>
        <source>Length</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>History</name>
    <message>
        <location filename="../history.cpp" line="148"/>
        <source>View Chat History</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../history.cpp" line="157"/>
        <source>Clear History</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../history.cpp" line="202"/>
        <source>Show last %1 messages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../history.cpp" line="206"/>
        <source>Show messages since yesterday</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../history.cpp" line="207"/>
        <source>Show messages from last 7 days</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../history.cpp" line="208"/>
        <source>Show messages from last 30 days</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../history.cpp" line="209"/>
        <source>Show whole history</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../history.cpp" line="454"/>
        <source>%1 day(s) %2 hour(s)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HistoryChatsModel</name>
    <message>
        <location filename="../model/history-chats-model.cpp" line="165"/>
        <source>Statuses</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../model/history-chats-model.cpp" line="195"/>
        <source>SMSes</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HistoryWindow</name>
    <message>
        <location filename="../gui/windows/history-window.cpp" line="79"/>
        <source>History</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/windows/history-window.cpp" line="88"/>
        <source>&amp;Remove entries</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/windows/history-window.cpp" line="185"/>
        <source>Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/windows/history-window.cpp" line="191"/>
        <source>by date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/windows/history-window.cpp" line="195"/>
        <source>From</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/windows/history-window.cpp" line="205"/>
        <source>To</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/windows/history-window.cpp" line="627"/>
        <source>&amp;Clear Chat History</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/windows/history-window.cpp" line="641"/>
        <source>&amp;Clear Status History</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/windows/history-window.cpp" line="654"/>
        <source>&amp;Clear SMS History</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/windows/history-window.cpp" line="689"/>
        <source>Kadu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/windows/history-window.cpp" line="689"/>
        <source>There is no history storage module loaded!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="../buddy-history-delete-handler.cpp" line="63"/>
        <source>Chat history</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SmsDatesModel</name>
    <message>
        <location filename="../model/sms-dates-model.cpp" line="62"/>
        <source>Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../model/sms-dates-model.cpp" line="63"/>
        <source>Length</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
