<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="pl">
<context>
    <name>@default</name>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="4"/>
        <location filename="../.configuration-ui-translations.cpp" line="55"/>
        <source>Chat</source>
        <translation>Rozmowa</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="7"/>
        <source>History</source>
        <translation>Historia</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="10"/>
        <source>Chats History</source>
        <translation>Historia rozmów</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="13"/>
        <location filename="../.configuration-ui-translations.cpp" line="15"/>
        <source>Save messages in history</source>
        <translation>Zapisuj wiadomości w historii</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="18"/>
        <source>Save chats with anonymous users</source>
        <translation>Zapisuj rozmowy z anonimami</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="22"/>
        <source>Status Changes</source>
        <translation>Zmiany statusów</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="25"/>
        <source>Save status changes</source>
        <translation>Zapisuj zmiany statusów</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="27"/>
        <source>Save status changes in history</source>
        <translation>Zapisuje zmiany statusów w historii</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="30"/>
        <source>Save status only with description</source>
        <translation>Zapisuj tylko statusy z opisem</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="34"/>
        <source>Quotation</source>
        <translation>Cytowanie</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="37"/>
        <source>Quote message in chat window</source>
        <translation>Cytowanie historii w oknie rozmowy</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="40"/>
        <source>Don&apos;t quote messages older than</source>
        <translation>Nie cytuj historii starszej niż</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="43"/>
        <source>.</source>
        <translation>.</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="49"/>
        <source>Shortcuts</source>
        <translation>Skróty</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="52"/>
        <source>General</source>
        <translation>Ogólne</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="58"/>
        <source>View history</source>
        <translation>Pokaż historię</translation>
    </message>
</context>
<context>
    <name>BuddyStatusDatesModel</name>
    <message>
        <location filename="../model/buddy-status-dates-model.cpp" line="62"/>
        <source>Date</source>
        <translation>Data</translation>
    </message>
    <message>
        <location filename="../model/buddy-status-dates-model.cpp" line="63"/>
        <source>Length</source>
        <translation>Długość</translation>
    </message>
</context>
<context>
    <name>ChatDatesModel</name>
    <message>
        <location filename="../model/chat-dates-model.cpp" line="65"/>
        <source>Chat</source>
        <translation>Rozmowa</translation>
    </message>
    <message>
        <location filename="../model/chat-dates-model.cpp" line="66"/>
        <source>Title</source>
        <translation>Tytuł</translation>
    </message>
    <message>
        <location filename="../model/chat-dates-model.cpp" line="67"/>
        <source>Date</source>
        <translation>Data</translation>
    </message>
    <message>
        <location filename="../model/chat-dates-model.cpp" line="68"/>
        <source>Length</source>
        <translation>Długość</translation>
    </message>
</context>
<context>
    <name>History</name>
    <message>
        <location filename="../history.cpp" line="148"/>
        <source>View Chat History</source>
        <translation>Pokaż historię rozmów</translation>
    </message>
    <message>
        <location filename="../history.cpp" line="157"/>
        <source>Clear History</source>
        <translation>Wyczyść historię</translation>
    </message>
    <message>
        <location filename="../history.cpp" line="202"/>
        <source>Show last %1 messages</source>
        <translation>Wyświetl ostatnie %1 wiadomości</translation>
    </message>
    <message>
        <location filename="../history.cpp" line="206"/>
        <source>Show messages since yesterday</source>
        <translation>Wyświetl wiadomości od wczoraj</translation>
    </message>
    <message>
        <location filename="../history.cpp" line="207"/>
        <source>Show messages from last 7 days</source>
        <translation>Wyświetl wiadomości z ostatniego tygodnia</translation>
    </message>
    <message>
        <location filename="../history.cpp" line="208"/>
        <source>Show messages from last 30 days</source>
        <translation>Wyświetl wiadomości z ostatnich 30 dni</translation>
    </message>
    <message>
        <location filename="../history.cpp" line="209"/>
        <source>Show whole history</source>
        <translation>Wyświetl całą historię</translation>
    </message>
    <message>
        <location filename="../history.cpp" line="454"/>
        <source>%1 day(s) %2 hour(s)</source>
        <translation>%1 dzień(dni) %2 godzina(y)</translation>
    </message>
</context>
<context>
    <name>HistoryChatsModel</name>
    <message>
        <location filename="../model/history-chats-model.cpp" line="165"/>
        <source>Statuses</source>
        <translation>Statusy</translation>
    </message>
    <message>
        <location filename="../model/history-chats-model.cpp" line="195"/>
        <source>SMSes</source>
        <translation>SMSy</translation>
    </message>
</context>
<context>
    <name>HistoryWindow</name>
    <message>
        <location filename="../gui/windows/history-window.cpp" line="79"/>
        <source>History</source>
        <translation>Historia</translation>
    </message>
    <message>
        <location filename="../gui/windows/history-window.cpp" line="88"/>
        <source>&amp;Remove entries</source>
        <translation>&amp;Usuń wpisy</translation>
    </message>
    <message>
        <location filename="../gui/windows/history-window.cpp" line="185"/>
        <source>Search</source>
        <translation>Szukaj</translation>
    </message>
    <message>
        <location filename="../gui/windows/history-window.cpp" line="191"/>
        <source>by date</source>
        <translation>Po dacie</translation>
    </message>
    <message>
        <location filename="../gui/windows/history-window.cpp" line="195"/>
        <source>From</source>
        <translation>od</translation>
    </message>
    <message>
        <location filename="../gui/windows/history-window.cpp" line="205"/>
        <source>To</source>
        <translation>do</translation>
    </message>
    <message>
        <location filename="../gui/windows/history-window.cpp" line="627"/>
        <source>&amp;Clear Chat History</source>
        <translation>&amp;Wyczyść historię rozmów</translation>
    </message>
    <message>
        <location filename="../gui/windows/history-window.cpp" line="641"/>
        <source>&amp;Clear Status History</source>
        <translation>&amp;Wyczyść historię statusów</translation>
    </message>
    <message>
        <location filename="../gui/windows/history-window.cpp" line="654"/>
        <source>&amp;Clear SMS History</source>
        <translation>&amp;Wyczyść historię SMSów</translation>
    </message>
    <message>
        <location filename="../gui/windows/history-window.cpp" line="689"/>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <location filename="../gui/windows/history-window.cpp" line="689"/>
        <source>There is no history storage module loaded!</source>
        <translation>Nie jest załadowany moduł przechowujący historię!</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="../buddy-history-delete-handler.cpp" line="63"/>
        <source>Chat history</source>
        <translation>Historia rozmów</translation>
    </message>
</context>
<context>
    <name>SmsDatesModel</name>
    <message>
        <location filename="../model/sms-dates-model.cpp" line="62"/>
        <source>Date</source>
        <translation>Data</translation>
    </message>
    <message>
        <location filename="../model/sms-dates-model.cpp" line="63"/>
        <source>Length</source>
        <translation>Długość</translation>
    </message>
</context>
</TS>
