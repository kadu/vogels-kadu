<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="pl">
<context>
    <name>ImportProfileWindow</name>
    <message>
        <location filename="../gui/windows/import-profile-window.cpp" line="57"/>
        <source>Select profile path</source>
        <translation>Wybierz ścieżkę do profilu</translation>
    </message>
    <message>
        <location filename="../gui/windows/import-profile-window.cpp" line="58"/>
        <source>Select profile path:</source>
        <translation>Wybierz ścieżkę do profilu:</translation>
    </message>
    <message>
        <location filename="../gui/windows/import-profile-window.cpp" line="62"/>
        <source>Select imported account identity:</source>
        <translation>Wybierz tożsamość dla importowanego konta:</translation>
    </message>
    <message>
        <location filename="../gui/windows/import-profile-window.cpp" line="65"/>
        <source>Import history</source>
        <translation>Importuj historię</translation>
    </message>
    <message>
        <location filename="../gui/windows/import-profile-window.cpp" line="75"/>
        <source>Import</source>
        <translation>Import</translation>
    </message>
    <message>
        <location filename="../gui/windows/import-profile-window.cpp" line="79"/>
        <source>Close</source>
        <translation>Zamknij</translation>
    </message>
    <message>
        <location filename="../gui/windows/import-profile-window.cpp" line="94"/>
        <source>&lt;b&gt;Identity not selected&lt;/b&gt;</source>
        <translation>&lt;b&gt;Nie wybrano tożsamości&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../gui/windows/import-profile-window.cpp" line="101"/>
        <source>&lt;b&gt;Selected directory does not contain kadu.conf.xml file&lt;/b&gt;</source>
        <translation>&lt;b&gt;Wybrany katalog nie zawiera pliku kadu.conf.xml&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../gui/windows/import-profile-window.cpp" line="114"/>
        <location filename="../gui/windows/import-profile-window.cpp" line="121"/>
        <location filename="../gui/windows/import-profile-window.cpp" line="132"/>
        <source>Import external profile...</source>
        <translation>Importuj zewnętrzny profil...</translation>
    </message>
    <message>
        <location filename="../gui/windows/import-profile-window.cpp" line="114"/>
        <source>This directory is not a Kadu profile directory.
File kadu.conf.xml not found</source>
        <translation>Ten katalog nie zawiera poprawnego profilu Kadu.
Plik kadu.conf.xml nie został odnaleziony</translation>
    </message>
    <message>
        <location filename="../gui/windows/import-profile-window.cpp" line="121"/>
        <source>Profile successfully imported!</source>
        <translation>Import profilu zakończony sukcesem!</translation>
    </message>
    <message>
        <location filename="../gui/windows/import-profile-window.cpp" line="132"/>
        <source>Unable to import profile: %1</source>
        <translation>Nie można zaimportować profilu: %1</translation>
    </message>
</context>
<context>
    <name>ImportProfilesWindow</name>
    <message>
        <location filename="../gui/windows/import-profiles-window.cpp" line="59"/>
        <source>&lt;p&gt;Current version of Kadu does not support user profiles.&lt;br /&gt;Instead, multiple account are supported in one instances of kadu.&lt;/p&gt;&lt;p&gt;Please select profiles that you would like to import as&lt;br /&gt;account into this instance of Kadu.&lt;/p&gt;</source>
        <translation>&lt;p&gt; Aktualna wersja Kadu nie obsługuje profili użytkowników.&lt;br /&gt;Zamiast tego, wiele kont jest obsługiwanych w jednej instancji Kadu. &lt;/p&gt;&lt;p&gt; Proszę wybrać profile, które chcesz zaimportować jako&lt;br /&gt;konta do tej instancji Kadu. &lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../gui/windows/import-profiles-window.cpp" line="69"/>
        <source>Import</source>
        <translation>Import</translation>
    </message>
    <message>
        <location filename="../gui/windows/import-profiles-window.cpp" line="73"/>
        <source>Close</source>
        <translation>Zamknij</translation>
    </message>
    <message>
        <location filename="../gui/windows/import-profiles-window.cpp" line="91"/>
        <source>Import history</source>
        <translation>Importuj historię</translation>
    </message>
    <message>
        <location filename="../gui/windows/import-profiles-window.cpp" line="118"/>
        <source>Import external profile...</source>
        <translation>Importuj zewnętrzny profil...</translation>
    </message>
    <message>
        <location filename="../gui/windows/import-profiles-window.cpp" line="118"/>
        <source>Profile %1 successfully imported!</source>
        <translation>Import profilu %1 zakończony sukcesem!</translation>
    </message>
    <message>
        <location filename="../gui/windows/import-profiles-window.cpp" line="130"/>
        <source>Import profile...</source>
        <translation>Importuj profil</translation>
    </message>
    <message>
        <location filename="../gui/windows/import-profiles-window.cpp" line="130"/>
        <source>Unable to import profile: %1: %2</source>
        <translation>Nie można zaimportować profilu: %1: %2</translation>
    </message>
</context>
<context>
    <name>ProfileImporter</name>
    <message>
        <location filename="../profile-importer.cpp" line="47"/>
        <source>Unable to open profile file [%1].</source>
        <translation>Nie można otworzyć pliku profilu: [%1].</translation>
    </message>
    <message>
        <location filename="../profile-importer.cpp" line="59"/>
        <source>Account already exists.</source>
        <translation>Konto już istnieje.</translation>
    </message>
    <message>
        <location filename="../profile-importer.cpp" line="66"/>
        <source>Imported account has no ID</source>
        <translation>Zaimportowane konto nie posiada ID</translation>
    </message>
</context>
<context>
    <name>ProfilesImportActions</name>
    <message>
        <location filename="../profiles-import-actions.cpp" line="56"/>
        <source>Import profiles...</source>
        <translation>Importuj profil...</translation>
    </message>
    <message>
        <location filename="../profiles-import-actions.cpp" line="62"/>
        <source>Import external profile...</source>
        <translation>Importuj zewnętrzny profil...</translation>
    </message>
</context>
</TS>
