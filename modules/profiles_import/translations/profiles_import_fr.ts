<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fr">
<context>
    <name>ImportProfileWindow</name>
    <message>
        <location filename="../gui/windows/import-profile-window.cpp" line="57"/>
        <source>Select profile path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/windows/import-profile-window.cpp" line="58"/>
        <source>Select profile path:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/windows/import-profile-window.cpp" line="62"/>
        <source>Select imported account identity:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/windows/import-profile-window.cpp" line="65"/>
        <source>Import history</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/windows/import-profile-window.cpp" line="75"/>
        <source>Import</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/windows/import-profile-window.cpp" line="79"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/windows/import-profile-window.cpp" line="94"/>
        <source>&lt;b&gt;Identity not selected&lt;/b&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/windows/import-profile-window.cpp" line="101"/>
        <source>&lt;b&gt;Selected directory does not contain kadu.conf.xml file&lt;/b&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/windows/import-profile-window.cpp" line="114"/>
        <location filename="../gui/windows/import-profile-window.cpp" line="121"/>
        <location filename="../gui/windows/import-profile-window.cpp" line="132"/>
        <source>Import external profile...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/windows/import-profile-window.cpp" line="114"/>
        <source>This directory is not a Kadu profile directory.
File kadu.conf.xml not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/windows/import-profile-window.cpp" line="121"/>
        <source>Profile successfully imported!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/windows/import-profile-window.cpp" line="132"/>
        <source>Unable to import profile: %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ImportProfilesWindow</name>
    <message>
        <location filename="../gui/windows/import-profiles-window.cpp" line="59"/>
        <source>&lt;p&gt;Current version of Kadu does not support user profiles.&lt;br /&gt;Instead, multiple account are supported in one instances of kadu.&lt;/p&gt;&lt;p&gt;Please select profiles that you would like to import as&lt;br /&gt;account into this instance of Kadu.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/windows/import-profiles-window.cpp" line="69"/>
        <source>Import</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/windows/import-profiles-window.cpp" line="73"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/windows/import-profiles-window.cpp" line="91"/>
        <source>Import history</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/windows/import-profiles-window.cpp" line="118"/>
        <source>Import external profile...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/windows/import-profiles-window.cpp" line="118"/>
        <source>Profile %1 successfully imported!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/windows/import-profiles-window.cpp" line="130"/>
        <source>Import profile...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/windows/import-profiles-window.cpp" line="130"/>
        <source>Unable to import profile: %1: %2</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ProfileImporter</name>
    <message>
        <location filename="../profile-importer.cpp" line="47"/>
        <source>Unable to open profile file [%1].</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../profile-importer.cpp" line="59"/>
        <source>Account already exists.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../profile-importer.cpp" line="66"/>
        <source>Imported account has no ID</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ProfilesImportActions</name>
    <message>
        <location filename="../profiles-import-actions.cpp" line="56"/>
        <source>Import profiles...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../profiles-import-actions.cpp" line="62"/>
        <source>Import external profile...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
