<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="pl">
<context>
    <name>@default</name>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="4"/>
        <source>Chat</source>
        <translation>Rozmowa</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="7"/>
        <source>Messages</source>
        <translation>Wiadomości</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="10"/>
        <source>Split messages</source>
        <translation>Dzielenie wiadomości</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="13"/>
        <source>Split too long messages</source>
        <translation>Dziel zbyt długie wiadomości na mniejsze</translation>
    </message>
</context>
</TS>
