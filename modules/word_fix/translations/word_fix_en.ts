<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="en_US">
<context>
    <name>@default</name>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="4"/>
        <source>Chat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="7"/>
        <location filename="../.configuration-ui-translations.cpp" line="10"/>
        <source>Words fix</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="13"/>
        <source>Enable word fix</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WordFix</name>
    <message>
        <location filename="../word_fix.cpp" line="365"/>
        <source>A word to be replaced</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../word_fix.cpp" line="369"/>
        <source>Value to replace with</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../word_fix.cpp" line="373"/>
        <source>Add</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../word_fix.cpp" line="374"/>
        <source>Change</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../word_fix.cpp" line="375"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../word_fix.cpp" line="400"/>
        <source>Word</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../word_fix.cpp" line="400"/>
        <source>Replace with</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
