<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="pl">
<context>
    <name>@default</name>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="4"/>
        <source>Chat</source>
        <translation>Rozmowa</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="7"/>
        <location filename="../.configuration-ui-translations.cpp" line="10"/>
        <source>Words fix</source>
        <translation>Korekta słów</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="13"/>
        <source>Enable word fix</source>
        <translation>Włącz korektę słów</translation>
    </message>
</context>
<context>
    <name>WordFix</name>
    <message>
        <location filename="../word_fix.cpp" line="365"/>
        <source>A word to be replaced</source>
        <translation>Słowo do zastąpienia</translation>
    </message>
    <message>
        <location filename="../word_fix.cpp" line="369"/>
        <source>Value to replace with</source>
        <translation>Wartość na jaką zmienić</translation>
    </message>
    <message>
        <location filename="../word_fix.cpp" line="373"/>
        <source>Add</source>
        <translation>Dodaj</translation>
    </message>
    <message>
        <location filename="../word_fix.cpp" line="374"/>
        <source>Change</source>
        <translation>Zmień</translation>
    </message>
    <message>
        <location filename="../word_fix.cpp" line="375"/>
        <source>Delete</source>
        <translation>Usuń</translation>
    </message>
    <message>
        <location filename="../word_fix.cpp" line="400"/>
        <source>Word</source>
        <translation>Słowo</translation>
    </message>
    <message>
        <location filename="../word_fix.cpp" line="400"/>
        <source>Replace with</source>
        <translation>Zamień na</translation>
    </message>
</context>
</TS>
