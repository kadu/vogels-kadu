<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="en_US">
<context>
    <name>@default</name>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="4"/>
        <source>Server Monitor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="7"/>
        <source>General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="10"/>
        <source>Server List Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="13"/>
        <source>Watch gadu-gadu servers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="16"/>
        <source>Server list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="20"/>
        <source>General Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="23"/>
        <source>Show reset button</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="26"/>
        <source>Auto refresh list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="29"/>
        <source>Refresh interval</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <location filename="../.configuration-ui-translations.cpp" line="31"/>
        <source>%n minute(s)</source>
        <translation>
            <numerusform>%n minute</numerusform>
            <numerusform>%n minutes</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>ServerMonitor</name>
    <message>
        <location filename="../server-monitor.cpp" line="82"/>
        <source>Server&apos;s Monitor</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ServerMonitorWindow</name>
    <message>
        <location filename="../server-monitor-window.cpp" line="46"/>
        <source>Refresh</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../server-monitor-window.cpp" line="53"/>
        <location filename="../server-monitor-window.cpp" line="155"/>
        <source>No information available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../server-monitor-window.cpp" line="60"/>
        <source>Server monitor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../server-monitor-window.cpp" line="104"/>
        <source>Available	%1
Unavailable	%2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../server-monitor-window.cpp" line="153"/>
        <source>Cannot read server list!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ServerStatusWidget</name>
    <message>
        <location filename="../server-status-widget.cpp" line="107"/>
        <source>Server %1 changed status to %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../server-status-widget.cpp" line="118"/>
        <source>Online</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../server-status-widget.cpp" line="121"/>
        <source>Unavailable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../server-status-widget.cpp" line="124"/>
        <source>Unknown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../server-status-widget.cpp" line="127"/>
        <source>Empty</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
