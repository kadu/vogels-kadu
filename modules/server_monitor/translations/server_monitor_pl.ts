<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="pl">
<context>
    <name>@default</name>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="4"/>
        <source>Server Monitor</source>
        <translation>Monitor serwerów</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="7"/>
        <source>General</source>
        <translation>Ogólne</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="10"/>
        <source>Server List Settings</source>
        <translation>Ustawienia listy serwerów</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="13"/>
        <source>Watch gadu-gadu servers</source>
        <translation>Obserwuj serwery Gadu-Gadu</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="16"/>
        <source>Server list</source>
        <translation>Lista serwerów</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="20"/>
        <source>General Settings</source>
        <translation>Ogólne ustawienia</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="23"/>
        <source>Show reset button</source>
        <translation>Pokaż przycisk odświeżania</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="26"/>
        <source>Auto refresh list</source>
        <translation>Automatycznie aktualizuj listę</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="29"/>
        <source>Refresh interval</source>
        <translation>Częstotliwość odświeżania</translation>
    </message>
    <message numerus="yes">
        <location filename="../.configuration-ui-translations.cpp" line="31"/>
        <source>%n minute(s)</source>
        <translation>
            <numerusform>%n minuta</numerusform>
            <numerusform>%n minuty</numerusform>
            <numerusform>%n minut</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>ServerMonitor</name>
    <message>
        <location filename="../server-monitor.cpp" line="82"/>
        <source>Server&apos;s Monitor</source>
        <translation>Monitor serwerów</translation>
    </message>
</context>
<context>
    <name>ServerMonitorWindow</name>
    <message>
        <location filename="../server-monitor-window.cpp" line="46"/>
        <source>Refresh</source>
        <translation>Odśwież</translation>
    </message>
    <message>
        <location filename="../server-monitor-window.cpp" line="53"/>
        <location filename="../server-monitor-window.cpp" line="155"/>
        <source>No information available</source>
        <translation>Brak dostępnych informacji</translation>
    </message>
    <message>
        <location filename="../server-monitor-window.cpp" line="60"/>
        <source>Server monitor</source>
        <translation>Monitor serwerów</translation>
    </message>
    <message>
        <location filename="../server-monitor-window.cpp" line="104"/>
        <source>Available	%1
Unavailable	%2</source>
        <translation>Dostępne	%1
Niedostępne %2</translation>
    </message>
    <message>
        <location filename="../server-monitor-window.cpp" line="153"/>
        <source>Cannot read server list!</source>
        <translation>Nie można odczytać listy serwerów!</translation>
    </message>
</context>
<context>
    <name>ServerStatusWidget</name>
    <message>
        <location filename="../server-status-widget.cpp" line="107"/>
        <source>Server %1 changed status to %2</source>
        <translation>Serwer %1 zmienił status na %2</translation>
    </message>
    <message>
        <location filename="../server-status-widget.cpp" line="118"/>
        <source>Online</source>
        <translation>Dostępny</translation>
    </message>
    <message>
        <location filename="../server-status-widget.cpp" line="121"/>
        <source>Unavailable</source>
        <translation>Niedostępny</translation>
    </message>
    <message>
        <location filename="../server-status-widget.cpp" line="124"/>
        <source>Unknown</source>
        <translation>Nieznany</translation>
    </message>
    <message>
        <location filename="../server-status-widget.cpp" line="127"/>
        <source>Empty</source>
        <translation>Pusty</translation>
    </message>
</context>
</TS>
