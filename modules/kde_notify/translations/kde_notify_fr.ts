<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fr">
<context>
    <name>@default</name>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="4"/>
        <source>Notifications</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="7"/>
        <source>KNotify</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="10"/>
        <source>General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="13"/>
        <source>Timeout</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <location filename="../.configuration-ui-translations.cpp" line="15"/>
        <source>%n second(s)</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="18"/>
        <source>Show message content in hint</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="21"/>
        <source>Number of quoted characters</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>KdeNotify</name>
    <message>
        <location filename="../kde_notify.cpp" line="67"/>
        <source>KDE4 notifications</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
