<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="pl">
<context>
    <name>@default</name>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="4"/>
        <source>Chat</source>
        <translation>Rozmowa</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="7"/>
        <source>SpellChecker</source>
        <translation>SpellChecker</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="10"/>
        <source>Misspelled Words Marking Options</source>
        <translation>Opcje zaznaczania błędnych słów</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="13"/>
        <source>Color</source>
        <translation>Kolor</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="16"/>
        <source>Bold</source>
        <translation>Pogrubienie</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="19"/>
        <source>Italic</source>
        <translation>Kursywa</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="22"/>
        <source>Underline</source>
        <translation>Podkreślenie</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="27"/>
        <location filename="../spellchecker.cpp" line="310"/>
        <source>Spell Checker Options</source>
        <translation>Opcje sprawdzania pisowni</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="31"/>
        <source>Ignore case</source>
        <translation>Ignoruj wielkość liter</translation>
    </message>
</context>
<context>
    <name>SpellChecker</name>
    <message>
        <location filename="../spellchecker.cpp" line="173"/>
        <location filename="../spellchecker.cpp" line="185"/>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <location filename="../spellchecker.cpp" line="316"/>
        <source>Move to &apos;Checked&apos;</source>
        <translation>Przenieś do &apos;Sprawdzane&apos;</translation>
    </message>
    <message>
        <location filename="../spellchecker.cpp" line="318"/>
        <source>Available languages</source>
        <translation>Dostępne języki</translation>
    </message>
    <message>
        <location filename="../spellchecker.cpp" line="323"/>
        <source>Move to &apos;Available languages&apos;</source>
        <translation>Przenieś do &apos;Dostępne języki&apos;</translation>
    </message>
    <message>
        <location filename="../spellchecker.cpp" line="325"/>
        <source>Checked</source>
        <translation>Sprawdzane</translation>
    </message>
</context>
</TS>
