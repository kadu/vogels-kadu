<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="pl">
<context>
    <name>@default</name>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="5"/>
        <source>Notifications</source>
        <translation>Powiadomienia</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="9"/>
        <source>Sound</source>
        <translation>Dźwięk</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="12"/>
        <source>Sound devices</source>
        <translation>Urządzenia dźwiękowe</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="15"/>
        <source>Playback device</source>
        <translation>Urządzenie odgrywające</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="18"/>
        <source>Recording device</source>
        <translation>Urządzenie nagrywające</translation>
    </message>
</context>
<context>
    <name>WIN32PlayerSlots</name>
    <message>
        <location filename="../win32_sound.cpp" line="84"/>
        <location filename="../win32_sound.cpp" line="105"/>
        <source>Default</source>
        <translation>Domyślne</translation>
    </message>
</context>
</TS>
