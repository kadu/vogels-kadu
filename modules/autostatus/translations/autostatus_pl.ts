<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="pl">
<context>
    <name>@default</name>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="4"/>
        <location filename="../.configuration-ui-translations.cpp" line="21"/>
        <source>Status</source>
        <translation>Status</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="7"/>
        <source>Autostatus</source>
        <translation>Automatyczny status</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="10"/>
        <source>General</source>
        <translation>Ogólne</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="13"/>
        <source>Autostatus file</source>
        <translation>Plik z automatycznymi statusami</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="16"/>
        <source>Time</source>
        <translation>Czas</translation>
    </message>
    <message numerus="yes">
        <location filename="../.configuration-ui-translations.cpp" line="18"/>
        <source>%n second(s)</source>
        <translation>
            <numerusform>%n sekunda</numerusform>
            <numerusform>%n sekundy</numerusform>
            <numerusform>%n sekund</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="24"/>
        <source>Online</source>
        <translation>Dostępny</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="27"/>
        <source>Busy</source>
        <translation>Zajęty</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="30"/>
        <source>Invisible</source>
        <translation>Niewidoczny</translation>
    </message>
</context>
<context>
    <name>AutostatusActions</name>
    <message>
        <location filename="../autostatus-actions.cpp" line="56"/>
        <source>&amp;Autostatus</source>
        <translation>&amp;Automatyczny status</translation>
    </message>
</context>
</TS>
