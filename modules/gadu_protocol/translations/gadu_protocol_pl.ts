<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="pl">
<context>
    <name>@default</name>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="4"/>
        <source>Accounts</source>
        <translation>Konta</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="7"/>
        <source>Account</source>
        <translation>Konto</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="10"/>
        <location filename="../.configuration-ui-translations.cpp" line="44"/>
        <source>General</source>
        <translation>Ogólne</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="13"/>
        <source>Name</source>
        <translation>Imię</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="17"/>
        <source>Gadu-Gadu</source>
        <translation>Gadu-Gadu</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="20"/>
        <source>UIN</source>
        <translation>Numer Gadu-Gadu</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="23"/>
        <source>Password</source>
        <translation>Hasło</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="27"/>
        <source>New Account</source>
        <translation>Nowe konto</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="30"/>
        <source>E-mail</source>
        <translation>Adres E-mail</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="33"/>
        <source>New password</source>
        <translation>Nowe hasło</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="36"/>
        <source>Register new account...</source>
        <translation>Zarejestruj nowe konto...</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="41"/>
        <location filename="../.configuration-ui-translations.cpp" line="51"/>
        <source>File Transfers</source>
        <translation>Transfery plików</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="47"/>
        <source>Allow file transfers</source>
        <translation>Zezwól na transfery plików</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="54"/>
        <source>Remove completed transfers from transfers list</source>
        <translation>Usuń ukończone transfery z listy</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="58"/>
        <source>IP</source>
        <translation>Adres IP</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="61"/>
        <source>DCC IP autodetection</source>
        <translation>Automatyczne wykrywanie adresu IP dla DCC</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="64"/>
        <source>IP address</source>
        <translation>Adres IP</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="67"/>
        <source>DCC forwarding enabled</source>
        <translation>Używaj przekierowania portów</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="70"/>
        <source>External IP address</source>
        <translation>Zewnętrzny adres IP</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="73"/>
        <source>External TCP port</source>
        <translation>Zewnętrzny port TCP</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="76"/>
        <source>Local TCP port</source>
        <translation>Lokalny port TCP</translation>
    </message>
</context>
<context>
    <name>GaduAddAccountWidget</name>
    <message>
        <location filename="../gui/widgets/gadu-add-account-widget.cpp" line="75"/>
        <source>Gadu-Gadu number</source>
        <translation>Numer Gadu-Gadu</translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-add-account-widget.cpp" line="80"/>
        <source>Password</source>
        <translation>Hasło</translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-add-account-widget.cpp" line="82"/>
        <source>Remember Password</source>
        <translation>Zapamiętaj hasło</translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-add-account-widget.cpp" line="92"/>
        <source>Account Identity</source>
        <translation>Tożsamość konta</translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-add-account-widget.cpp" line="94"/>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Select or enter the identity that will be associated with this account.&lt;/i&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Wybierz lub wprowadź tożsamość z która będzie skojarzone konto.&lt;/i&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-add-account-widget.cpp" line="105"/>
        <source>Add Account</source>
        <translation>Dodaj konto</translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-add-account-widget.cpp" line="106"/>
        <source>Cancel</source>
        <translation>Anuluj</translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-add-account-widget.cpp" line="161"/>
        <location filename="../gui/widgets/gadu-add-account-widget.cpp" line="166"/>
        <source>Forgot Your Password?</source>
        <translation>Zapomniałeś hasła?</translation>
    </message>
</context>
<context>
    <name>GaduChangePasswordWindow</name>
    <message>
        <location filename="../gui/windows/gadu-change-password-window.cpp" line="44"/>
        <location filename="../gui/windows/gadu-change-password-window.cpp" line="125"/>
        <source>Change Password</source>
        <translation>Zmień hasło</translation>
    </message>
    <message>
        <location filename="../gui/windows/gadu-change-password-window.cpp" line="62"/>
        <source>This dialog box allows you to change your current password.
</source>
        <translation>To okno dialogowe pozwala zmienić bieżące hasło.
</translation>
    </message>
    <message>
        <location filename="../gui/windows/gadu-change-password-window.cpp" line="75"/>
        <source>E-Mail Address</source>
        <translation>Adres e-mail</translation>
    </message>
    <message>
        <location filename="../gui/windows/gadu-change-password-window.cpp" line="77"/>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Type E-Mail Address used during registration.&lt;/i&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Podaj adres e-mail Address użyty podczas rejestracji konta.&lt;/i&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="../gui/windows/gadu-change-password-window.cpp" line="86"/>
        <source>Old Password</source>
        <translation>Stare hasło</translation>
    </message>
    <message>
        <location filename="../gui/windows/gadu-change-password-window.cpp" line="88"/>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Enter current password for your Gadu-Gadu account.&lt;/i&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Wprowadź stare hasło.&lt;/i&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="../gui/windows/gadu-change-password-window.cpp" line="97"/>
        <source>New Password</source>
        <translation>Nowe hasło</translation>
    </message>
    <message>
        <location filename="../gui/windows/gadu-change-password-window.cpp" line="99"/>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Enter new password for your Gadu-Gadu account.&lt;/i&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Wprowadź nowe hasło.&lt;/i&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="../gui/windows/gadu-change-password-window.cpp" line="108"/>
        <source>Retype New Password</source>
        <translation>Powtórz nowe hasło</translation>
    </message>
    <message>
        <location filename="../gui/windows/gadu-change-password-window.cpp" line="112"/>
        <source>Characters</source>
        <translation>Weryfikacja</translation>
    </message>
    <message>
        <location filename="../gui/windows/gadu-change-password-window.cpp" line="114"/>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;For verification purposes, please type the characters above.&lt;/i&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Wprowadź tekst widoczny na obrazku.&lt;/i&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="../gui/windows/gadu-change-password-window.cpp" line="126"/>
        <source>Cancel</source>
        <translation>Anuluj</translation>
    </message>
    <message>
        <location filename="../gui/windows/gadu-change-password-window.cpp" line="150"/>
        <location filename="../gui/windows/gadu-change-password-window.cpp" line="175"/>
        <location filename="../gui/windows/gadu-change-password-window.cpp" line="183"/>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <location filename="../gui/windows/gadu-change-password-window.cpp" line="150"/>
        <source>Error data typed in required fields.

Passwords typed in both fields (&quot;Password&quot; and &quot;Retype Password&quot;) should be the same!</source>
        <translation>Błąd w wymaganych danych.

Nowe hasło wprowadzone w polu &quot;Nowe hasło&quot; nie jest identyczne z zawartością pola &quot;Powtórz nowe hasło&quot;!</translation>
    </message>
    <message>
        <location filename="../gui/windows/gadu-change-password-window.cpp" line="175"/>
        <source>Changing password was successful.</source>
        <translation>Hasło zostało pomyślnie zmienione.</translation>
    </message>
    <message>
        <location filename="../gui/windows/gadu-change-password-window.cpp" line="183"/>
        <source>An error has occurred. Please try again later.</source>
        <translation>Wystąpił błąd. Proszę spróbować później</translation>
    </message>
</context>
<context>
    <name>GaduChatService</name>
    <message>
        <location filename="../services/gadu-chat-service.cpp" line="88"/>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <location filename="../services/gadu-chat-service.cpp" line="88"/>
        <source>Filtered message too long (%1&gt;=%2)</source>
        <translation>Wiadomość zbyt długa (%1&gt;=%2)</translation>
    </message>
</context>
<context>
    <name>GaduContactPersonalInfoWidget</name>
    <message>
        <location filename="../gui/widgets/gadu-contact-personal-info-widget.cpp" line="71"/>
        <source>First Name</source>
        <translation>Imię</translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-contact-personal-info-widget.cpp" line="74"/>
        <source>Last Name</source>
        <translation>Nazwisko</translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-contact-personal-info-widget.cpp" line="77"/>
        <source>Nickname</source>
        <translation>Pseudonim</translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-contact-personal-info-widget.cpp" line="80"/>
        <source>Gender</source>
        <translation>Płeć</translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-contact-personal-info-widget.cpp" line="83"/>
        <source>Birthdate</source>
        <translation>Data urodzenia</translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-contact-personal-info-widget.cpp" line="86"/>
        <source>City</source>
        <translation>Miasto</translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-contact-personal-info-widget.cpp" line="89"/>
        <source>State/Province</source>
        <translation>Stan/Region</translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-contact-personal-info-widget.cpp" line="92"/>
        <source>IP Address</source>
        <translation>Adres IP</translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-contact-personal-info-widget.cpp" line="95"/>
        <source>Port</source>
        <translation>Port</translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-contact-personal-info-widget.cpp" line="98"/>
        <source>DNS Name</source>
        <translation>Nazwa DNS</translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-contact-personal-info-widget.cpp" line="101"/>
        <source>Protocol Version</source>
        <translation>Wersja protokołu</translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-contact-personal-info-widget.cpp" line="139"/>
        <source>Female</source>
        <translation>Kobieta</translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-contact-personal-info-widget.cpp" line="142"/>
        <source>Male</source>
        <translation>Mężczyzna</translation>
    </message>
</context>
<context>
    <name>GaduCreateAccountWidget</name>
    <message>
        <location filename="../gui/widgets/gadu-create-account-widget.cpp" line="77"/>
        <source>Password</source>
        <translation>Hasło</translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-create-account-widget.cpp" line="82"/>
        <source>Retype Password</source>
        <translation>Powtórz hasło</translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-create-account-widget.cpp" line="84"/>
        <source>Remember password</source>
        <translation>Zapamiętaj hasło</translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-create-account-widget.cpp" line="89"/>
        <source>E-Mail Address</source>
        <translation>Adres e-mail</translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-create-account-widget.cpp" line="93"/>
        <source>Account Identity</source>
        <translation>Tożsamość konta</translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-create-account-widget.cpp" line="95"/>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Select or enter the identity that will be associated with this account.&lt;/i&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Wybierz lub wprowadź tożsamość z która będzie skojarzone konto.&lt;/i&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-create-account-widget.cpp" line="103"/>
        <source>Characters</source>
        <translation>Weryfikacja</translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-create-account-widget.cpp" line="105"/>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;For verification purposes, please type the characters above.&lt;/i&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Wprowadź tekst widoczny na obrazku.&lt;/i&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-create-account-widget.cpp" line="116"/>
        <source>Regster Account</source>
        <translation>Zarejestruj konto</translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-create-account-widget.cpp" line="117"/>
        <source>Cancel</source>
        <translation>Anuluj</translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-create-account-widget.cpp" line="168"/>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-create-account-widget.cpp" line="168"/>
        <source>Error data typed in required fields.

Passwords typed in both fields (&quot;Password&quot; and &quot;Retype Password&quot;) should be the same!</source>
        <translation>Błędne dane w  wymaganych polach.
Hasła  muszą być identyczne w obu polach!</translation>
    </message>
</context>
<context>
    <name>GaduEditAccountWidget</name>
    <message>
        <location filename="../gui/widgets/gadu-edit-account-widget.cpp" line="94"/>
        <source>Apply</source>
        <translation>Zastosuj</translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-edit-account-widget.cpp" line="97"/>
        <source>Cancel</source>
        <translation>Anuluj</translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-edit-account-widget.cpp" line="100"/>
        <source>Delete account</source>
        <translation>Usuń konto</translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-edit-account-widget.cpp" line="123"/>
        <source>Gadu-Gadu number</source>
        <translation>Numer Gadu-Gadu</translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-edit-account-widget.cpp" line="128"/>
        <source>Password</source>
        <translation>Hasło</translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-edit-account-widget.cpp" line="130"/>
        <source>Remember password</source>
        <translation>Zapamiętaj hasło</translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-edit-account-widget.cpp" line="135"/>
        <source>Forgot Your Password?</source>
        <translation>Zapomniałeś hasła?</translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-edit-account-widget.cpp" line="140"/>
        <source>Change Your Password</source>
        <translation>Zmień hasło</translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-edit-account-widget.cpp" line="147"/>
        <source>Account Identity</source>
        <translation>Tożsamość konta</translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-edit-account-widget.cpp" line="149"/>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Select or enter the identity that will be associated with this account.&lt;/i&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Wybierz lub wprowadź tożsamość z która będzie skojarzone konto.&lt;/i&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-edit-account-widget.cpp" line="158"/>
        <location filename="../gui/widgets/gadu-edit-account-widget.cpp" line="258"/>
        <source>General</source>
        <translation>Ogólne</translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-edit-account-widget.cpp" line="165"/>
        <source>Personal info</source>
        <translation>Informacje osobiste</translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-edit-account-widget.cpp" line="176"/>
        <source>Buddies</source>
        <translation>Znajomi</translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-edit-account-widget.cpp" line="182"/>
        <source>Connection</source>
        <translation>Połączenie</translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-edit-account-widget.cpp" line="187"/>
        <source>Proxy</source>
        <translation>Serwer proxy</translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-edit-account-widget.cpp" line="277"/>
        <source>Allow file transfers</source>
        <translation>Zezwól na transfery plików</translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-edit-account-widget.cpp" line="197"/>
        <source>Options</source>
        <translation>Opcje</translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-edit-account-widget.cpp" line="201"/>
        <source>Images</source>
        <translation>Obrazki</translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-edit-account-widget.cpp" line="209"/>
        <source>Maximum image size that we want to receive</source>
        <translation>Maksymalny rozmiar obrazka</translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-edit-account-widget.cpp" line="212"/>
        <source>Maximum image size for chat</source>
        <translation>Maksymalny rozmiar obrazka dla rozmowy</translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-edit-account-widget.cpp" line="214"/>
        <source>Receive images during invisibility</source>
        <translation>Odbieraj obrazki także gdy jestem ukryty</translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-edit-account-widget.cpp" line="215"/>
        <source>Receiving images during invisibility is allowed</source>
        <translation>Odbieranie obrazków w trybie ukrytym jest włączone</translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-edit-account-widget.cpp" line="224"/>
        <source>Define limit of images received per minute</source>
        <translation>Zdefiniuj maksymalną dozwoloną liczbę obrazków na minutę</translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-edit-account-widget.cpp" line="227"/>
        <source>Limit numbers of image received per minute</source>
        <translation>Limit liczby obrazków odbieranych w ciągu minuty</translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-edit-account-widget.cpp" line="231"/>
        <source>Status</source>
        <translation>Status</translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-edit-account-widget.cpp" line="234"/>
        <source>Show my status to everyone</source>
        <translation>Pokaż wszystkim mój status</translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-edit-account-widget.cpp" line="235"/>
        <source>When disabled, you&apos;re visible only to buddies on your list</source>
        <translation>Gdy wyłączone, jesteś widoczny tylko dla znajomych z twojej listy</translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-edit-account-widget.cpp" line="243"/>
        <source>Notifications</source>
        <translation>Powiadomienia</translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-edit-account-widget.cpp" line="246"/>
        <source>Send composing events</source>
        <translation>Wysyłaj powiadomienia o pisaniu</translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-edit-account-widget.cpp" line="264"/>
        <source>Use default servers</source>
        <translation>Użyj domyślnych serwerów</translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-edit-account-widget.cpp" line="267"/>
        <source>IP addresses</source>
        <translation>Adres IP</translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-edit-account-widget.cpp" line="288"/>
        <source>Use encrypted connection</source>
        <translation>Używaj szyfrowanego połączenia</translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-edit-account-widget.cpp" line="444"/>
        <source>Confirm account removal</source>
        <translation>Potwierdź usunięcie konta</translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-edit-account-widget.cpp" line="445"/>
        <source>Are you sure do you want to remove account %1 (%2)</source>
        <translation>Czy na pewno chcesz usunąć konto %1 (%2)</translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-edit-account-widget.cpp" line="449"/>
        <source>Remove account</source>
        <translation>Usuń konto</translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-edit-account-widget.cpp" line="450"/>
        <source>Remove account and unregister from server</source>
        <translation>Usuń konto i wyrejestruj z serwera</translation>
    </message>
</context>
<context>
    <name>GaduFormatter</name>
    <message>
        <location filename="../helpers/gadu-formatter.cpp" line="160"/>
        <source>###IMAGE BLOCKED###</source>
        <translation>###OBRAZEK ZABLOKOWANY###</translation>
    </message>
    <message>
        <location filename="../helpers/gadu-formatter.cpp" line="170"/>
        <source>###IMAGE TOO BIG###</source>
        <translation>###OBRAZEK ZBYT DUŻY###</translation>
    </message>
</context>
<context>
    <name>GaduPersonalInfoWidget</name>
    <message>
        <location filename="../gui/widgets/gadu-personal-info-widget.cpp" line="70"/>
        <source>Unknown Gender</source>
        <translation>Płeć nieokreślona</translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-personal-info-widget.cpp" line="71"/>
        <source>Male</source>
        <translation>Mężczyzna</translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-personal-info-widget.cpp" line="72"/>
        <source>Female</source>
        <translation>Kobieta</translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-personal-info-widget.cpp" line="87"/>
        <source>Nick</source>
        <translation>Pseudonim</translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-personal-info-widget.cpp" line="88"/>
        <source>First name</source>
        <translation>Imię</translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-personal-info-widget.cpp" line="89"/>
        <source>Last name</source>
        <translation>Nazwisko</translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-personal-info-widget.cpp" line="90"/>
        <source>Sex</source>
        <translation>Płeć</translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-personal-info-widget.cpp" line="91"/>
        <source>Family name</source>
        <translation>Nazwisko rodzinne</translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-personal-info-widget.cpp" line="92"/>
        <source>Birth year</source>
        <translation>Rok urodzenia</translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-personal-info-widget.cpp" line="93"/>
        <source>City</source>
        <translation>Miejscowość</translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-personal-info-widget.cpp" line="94"/>
        <source>Family city</source>
        <translation>Miejscowość rodzinna</translation>
    </message>
</context>
<context>
    <name>GaduProtocol</name>
    <message>
        <location filename="../gadu-protocol.cpp" line="399"/>
        <location filename="../gadu-protocol.cpp" line="679"/>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <location filename="../gadu-protocol.cpp" line="399"/>
        <source>UIN not set!</source>
        <translation>Numer Gadu-Gadu nie jest ustawiony!</translation>
    </message>
    <message>
        <location filename="../gadu-protocol.cpp" line="408"/>
        <source>Please provide password for %1 (%2) account</source>
        <translation>Wprowadź hasło dla konta %1 (%2)</translation>
    </message>
    <message>
        <location filename="../gadu-protocol.cpp" line="668"/>
        <source>Unable to connect, server has not been found</source>
        <translation>Połączenie niemożliwe, serwer nie znaleziony</translation>
    </message>
    <message>
        <location filename="../gadu-protocol.cpp" line="672"/>
        <source>Unable to connect</source>
        <translation>Połączenie niemożliwe</translation>
    </message>
    <message>
        <location filename="../gadu-protocol.cpp" line="676"/>
        <source>Please change your email in &quot;Change password / email&quot; window. Leave new password field blank.</source>
        <translation>Zmień swój e-mail w oknie &quot;Zmień hasło / e-mai&quot;. Pole nowe hasło pozostaw puste.</translation>
    </message>
    <message>
        <location filename="../gadu-protocol.cpp" line="683"/>
        <source>Unable to connect, server has returned unknown data</source>
        <translation>Połączenie niemożliwe, serwer odpowiedział nieznanymi danymi</translation>
    </message>
    <message>
        <location filename="../gadu-protocol.cpp" line="687"/>
        <source>Unable to connect, connection break during reading</source>
        <translation>Połączenie niemożliwe, połączenie przerwane w czasie odczytu</translation>
    </message>
    <message>
        <location filename="../gadu-protocol.cpp" line="691"/>
        <source>Unable to connect, connection break during writing</source>
        <translation>Połączenie niemożliwe, połączenie przerwane w czasie zapisu</translation>
    </message>
    <message>
        <location filename="../gadu-protocol.cpp" line="701"/>
        <source>Unable to connect, error of negotiation TLS</source>
        <translation>Połączenie niemożliwe, błąd negocjacji TLS</translation>
    </message>
    <message>
        <location filename="../gadu-protocol.cpp" line="705"/>
        <source>Too many connection attempts with bad password!</source>
        <translation>Zbyt wiele połączeń z użyciem błędnego hasła!</translation>
    </message>
    <message>
        <location filename="../gadu-protocol.cpp" line="710"/>
        <source>Unable to connect, servers are down</source>
        <translation>Połączenie nie możliwe, serwery są niedostępne</translation>
    </message>
    <message>
        <location filename="../gadu-protocol.cpp" line="714"/>
        <location filename="../gadu-protocol.cpp" line="728"/>
        <source>Connection broken</source>
        <translation>Zerwane połączenie</translation>
    </message>
    <message>
        <location filename="../gadu-protocol.cpp" line="719"/>
        <source>Connection timeout!</source>
        <translation>Przekroczenie czasu połączenia!</translation>
    </message>
    <message>
        <location filename="../gadu-protocol.cpp" line="723"/>
        <source>Disconnection has occurred</source>
        <translation>Nastąpiło rozłączenie</translation>
    </message>
</context>
<context>
    <name>GaduProtocolFactory</name>
    <message>
        <location filename="../gadu-protocol-factory.cpp" line="111"/>
        <source>Gadu-Gadu number:</source>
        <translation>Numer Gadu-Gadu:</translation>
    </message>
</context>
<context>
    <name>GaduRemindPasswordWindow</name>
    <message>
        <location filename="../gui/windows/gadu-remind-password-window.cpp" line="43"/>
        <source>Remind password</source>
        <translation>Przypomnij hasło</translation>
    </message>
    <message>
        <location filename="../gui/windows/gadu-remind-password-window.cpp" line="62"/>
        <source>This dialog box allows you to ask server to remind your current password.</source>
        <translation>To okno dialogowe pozwala zapytać serwer o przypomnienie bieżącego hasła.</translation>
    </message>
    <message>
        <location filename="../gui/windows/gadu-remind-password-window.cpp" line="76"/>
        <source>E-Mail Address</source>
        <translation>Adres e-mail</translation>
    </message>
    <message>
        <location filename="../gui/windows/gadu-remind-password-window.cpp" line="78"/>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Type E-Mail Address used during registration.&lt;/i&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Podaj adres e-mail Address użyty podczas rejestracji konta.&lt;/i&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="../gui/windows/gadu-remind-password-window.cpp" line="86"/>
        <source>Characters</source>
        <translation>Weryfikacja</translation>
    </message>
    <message>
        <location filename="../gui/windows/gadu-remind-password-window.cpp" line="88"/>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;For verification purposes, please type the characters above.&lt;/i&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Wprowadź tekst widoczny na obrazku.&lt;/i&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="../gui/windows/gadu-remind-password-window.cpp" line="99"/>
        <source>Send Password</source>
        <translation>Wyślij hasło</translation>
    </message>
    <message>
        <location filename="../gui/windows/gadu-remind-password-window.cpp" line="100"/>
        <source>Cancel</source>
        <translation>Anuluj</translation>
    </message>
    <message>
        <location filename="../gui/windows/gadu-remind-password-window.cpp" line="139"/>
        <location filename="../gui/windows/gadu-remind-password-window.cpp" line="143"/>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <location filename="../gui/windows/gadu-remind-password-window.cpp" line="139"/>
        <source>Your password has been send on your email</source>
        <translation>Twoje hasło zostało wysłane na maila</translation>
    </message>
    <message>
        <location filename="../gui/windows/gadu-remind-password-window.cpp" line="143"/>
        <source>Error during remind password</source>
        <translation>Bład podczas przypominania hasła</translation>
    </message>
</context>
<context>
    <name>GaduUnregisterAccountWindow</name>
    <message>
        <location filename="../gui/windows/gadu-unregister-account-window.cpp" line="44"/>
        <source>Unregister account</source>
        <translation>Wyrejestruj konto</translation>
    </message>
    <message>
        <location filename="../gui/windows/gadu-unregister-account-window.cpp" line="62"/>
        <source>This dialog box allows you to unregister your account. Be aware of using this option.</source>
        <translation>To okno pozwala na wyrejestrowanie konta. Uważaj z używaniem tej opcji.</translation>
    </message>
    <message>
        <location filename="../gui/windows/gadu-unregister-account-window.cpp" line="69"/>
        <source>&lt;font size=&quot;+1&quot;&gt;&lt;b&gt;It will permanently delete your account and you will not be able to use it later!&lt;/b&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&quot;+1&quot;&gt;&lt;b&gt;To okno dialogowe pozwala na trwałe wyrejestrowanie użytkownika z serwera Gadu-Gadu.&lt;/b&gt;&lt;/font&gt;&lt;br /&gt;&lt;font color=&quot;red&quot;&gt;&lt;b&gt;Musisz sobie zdawać sprawę że spowoduje to nieodwracalne usunięcie numeru GG z serwera i nigdy już nie będziesz mógł z niego korzystać!&lt;/b&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="../gui/windows/gadu-unregister-account-window.cpp" line="84"/>
        <source>Gadu-Gadu number</source>
        <translation>Numer Gadu-Gadu</translation>
    </message>
    <message>
        <location filename="../gui/windows/gadu-unregister-account-window.cpp" line="89"/>
        <source>Password</source>
        <translation>Hasło</translation>
    </message>
    <message>
        <location filename="../gui/windows/gadu-unregister-account-window.cpp" line="91"/>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Enter password for your Gadu-Gadu account.&lt;/i&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Podaj adres e-mail Address użyty podczas rejestracji konta.&lt;/i&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="../gui/windows/gadu-unregister-account-window.cpp" line="99"/>
        <source>Characters</source>
        <translation>Weryfikacja</translation>
    </message>
    <message>
        <location filename="../gui/windows/gadu-unregister-account-window.cpp" line="101"/>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;For verification purposes, please type the characters above.&lt;/i&gt;&lt;/font&gt;</source>
        <translation>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Wprowadź tekst widoczny na obrazku.&lt;/i&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="../gui/windows/gadu-unregister-account-window.cpp" line="112"/>
        <source>Unregister Account</source>
        <translation>Wyrejestruj konto</translation>
    </message>
    <message>
        <location filename="../gui/windows/gadu-unregister-account-window.cpp" line="113"/>
        <source>Cancel</source>
        <translation>Anuluj</translation>
    </message>
    <message>
        <location filename="../gui/windows/gadu-unregister-account-window.cpp" line="153"/>
        <location filename="../gui/windows/gadu-unregister-account-window.cpp" line="159"/>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <location filename="../gui/windows/gadu-unregister-account-window.cpp" line="153"/>
        <source>Unregistation was successful. Now you don&apos;t have any GG number :(</source>
        <translation>Wyrejestrowanie przebiegło prawidłowo. Teraz już nie masz numeru GG :(</translation>
    </message>
    <message>
        <location filename="../gui/windows/gadu-unregister-account-window.cpp" line="159"/>
        <source>An error has occurred while unregistration. Please try again later.</source>
        <translation>Wystąpił błąd podczas wyrejestrowywania konta. Proszę spróbować później.</translation>
    </message>
</context>
<context>
    <name>GaduWaitForAccountRegisterWindow</name>
    <message>
        <location filename="../gui/windows/gadu-wait-for-account-register-window.cpp" line="43"/>
        <source>Plase wait. New Gadu-Gadu account is being registered.</source>
        <translation>Proszę czekać. Nowe konto Gadu-Gadu jest rejestrowane.</translation>
    </message>
    <message>
        <location filename="../gui/windows/gadu-wait-for-account-register-window.cpp" line="55"/>
        <source>Registration was successful. Your new number is %1.
Store it in a safe place along with the password.
Now add your friends to the userlist.</source>
        <translation>Rejestracja zakończona. Twój nowy numer to: %1.
Zapisz go wraz z hasłem w bezpiecznym miejscu.
Teraz możesz dodać znajomych do listy kontaktów.</translation>
    </message>
    <message>
        <location filename="../gui/windows/gadu-wait-for-account-register-window.cpp" line="62"/>
        <source>An error has occurred during registration. Please try again later.</source>
        <translation>Wystąpił błąd podczas rejestracji. Proszę spróbować później.</translation>
    </message>
</context>
</TS>
