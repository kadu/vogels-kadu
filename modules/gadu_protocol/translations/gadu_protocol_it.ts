<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="it">
<context>
    <name>@default</name>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="4"/>
        <source>Accounts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="7"/>
        <source>Account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="10"/>
        <location filename="../.configuration-ui-translations.cpp" line="44"/>
        <source>General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="13"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="17"/>
        <source>Gadu-Gadu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="20"/>
        <source>UIN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="23"/>
        <source>Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="27"/>
        <source>New Account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="30"/>
        <source>E-mail</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="33"/>
        <source>New password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="36"/>
        <source>Register new account...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="41"/>
        <location filename="../.configuration-ui-translations.cpp" line="51"/>
        <source>File Transfers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="47"/>
        <source>Allow file transfers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="54"/>
        <source>Remove completed transfers from transfers list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="58"/>
        <source>IP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="61"/>
        <source>DCC IP autodetection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="64"/>
        <source>IP address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="67"/>
        <source>DCC forwarding enabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="70"/>
        <source>External IP address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="73"/>
        <source>External TCP port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="76"/>
        <source>Local TCP port</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GaduAddAccountWidget</name>
    <message>
        <location filename="../gui/widgets/gadu-add-account-widget.cpp" line="75"/>
        <source>Gadu-Gadu number</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-add-account-widget.cpp" line="80"/>
        <source>Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-add-account-widget.cpp" line="82"/>
        <source>Remember Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-add-account-widget.cpp" line="92"/>
        <source>Account Identity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-add-account-widget.cpp" line="94"/>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Select or enter the identity that will be associated with this account.&lt;/i&gt;&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-add-account-widget.cpp" line="105"/>
        <source>Add Account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-add-account-widget.cpp" line="106"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-add-account-widget.cpp" line="161"/>
        <location filename="../gui/widgets/gadu-add-account-widget.cpp" line="166"/>
        <source>Forgot Your Password?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GaduChangePasswordWindow</name>
    <message>
        <location filename="../gui/windows/gadu-change-password-window.cpp" line="44"/>
        <location filename="../gui/windows/gadu-change-password-window.cpp" line="125"/>
        <source>Change Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/windows/gadu-change-password-window.cpp" line="62"/>
        <source>This dialog box allows you to change your current password.
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/windows/gadu-change-password-window.cpp" line="75"/>
        <source>E-Mail Address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/windows/gadu-change-password-window.cpp" line="77"/>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Type E-Mail Address used during registration.&lt;/i&gt;&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/windows/gadu-change-password-window.cpp" line="86"/>
        <source>Old Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/windows/gadu-change-password-window.cpp" line="88"/>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Enter current password for your Gadu-Gadu account.&lt;/i&gt;&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/windows/gadu-change-password-window.cpp" line="97"/>
        <source>New Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/windows/gadu-change-password-window.cpp" line="99"/>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Enter new password for your Gadu-Gadu account.&lt;/i&gt;&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/windows/gadu-change-password-window.cpp" line="108"/>
        <source>Retype New Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/windows/gadu-change-password-window.cpp" line="112"/>
        <source>Characters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/windows/gadu-change-password-window.cpp" line="114"/>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;For verification purposes, please type the characters above.&lt;/i&gt;&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/windows/gadu-change-password-window.cpp" line="126"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/windows/gadu-change-password-window.cpp" line="150"/>
        <location filename="../gui/windows/gadu-change-password-window.cpp" line="175"/>
        <location filename="../gui/windows/gadu-change-password-window.cpp" line="183"/>
        <source>Kadu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/windows/gadu-change-password-window.cpp" line="150"/>
        <source>Error data typed in required fields.

Passwords typed in both fields (&quot;Password&quot; and &quot;Retype Password&quot;) should be the same!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/windows/gadu-change-password-window.cpp" line="175"/>
        <source>Changing password was successful.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/windows/gadu-change-password-window.cpp" line="183"/>
        <source>An error has occurred. Please try again later.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GaduChatService</name>
    <message>
        <location filename="../services/gadu-chat-service.cpp" line="88"/>
        <source>Kadu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../services/gadu-chat-service.cpp" line="88"/>
        <source>Filtered message too long (%1&gt;=%2)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GaduContactPersonalInfoWidget</name>
    <message>
        <location filename="../gui/widgets/gadu-contact-personal-info-widget.cpp" line="71"/>
        <source>First Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-contact-personal-info-widget.cpp" line="74"/>
        <source>Last Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-contact-personal-info-widget.cpp" line="77"/>
        <source>Nickname</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-contact-personal-info-widget.cpp" line="80"/>
        <source>Gender</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-contact-personal-info-widget.cpp" line="83"/>
        <source>Birthdate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-contact-personal-info-widget.cpp" line="86"/>
        <source>City</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-contact-personal-info-widget.cpp" line="89"/>
        <source>State/Province</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-contact-personal-info-widget.cpp" line="92"/>
        <source>IP Address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-contact-personal-info-widget.cpp" line="95"/>
        <source>Port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-contact-personal-info-widget.cpp" line="98"/>
        <source>DNS Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-contact-personal-info-widget.cpp" line="101"/>
        <source>Protocol Version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-contact-personal-info-widget.cpp" line="139"/>
        <source>Female</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-contact-personal-info-widget.cpp" line="142"/>
        <source>Male</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GaduCreateAccountWidget</name>
    <message>
        <location filename="../gui/widgets/gadu-create-account-widget.cpp" line="77"/>
        <source>Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-create-account-widget.cpp" line="82"/>
        <source>Retype Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-create-account-widget.cpp" line="84"/>
        <source>Remember password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-create-account-widget.cpp" line="89"/>
        <source>E-Mail Address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-create-account-widget.cpp" line="93"/>
        <source>Account Identity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-create-account-widget.cpp" line="95"/>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Select or enter the identity that will be associated with this account.&lt;/i&gt;&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-create-account-widget.cpp" line="103"/>
        <source>Characters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-create-account-widget.cpp" line="105"/>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;For verification purposes, please type the characters above.&lt;/i&gt;&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-create-account-widget.cpp" line="116"/>
        <source>Regster Account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-create-account-widget.cpp" line="117"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-create-account-widget.cpp" line="168"/>
        <source>Kadu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-create-account-widget.cpp" line="168"/>
        <source>Error data typed in required fields.

Passwords typed in both fields (&quot;Password&quot; and &quot;Retype Password&quot;) should be the same!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GaduEditAccountWidget</name>
    <message>
        <location filename="../gui/widgets/gadu-edit-account-widget.cpp" line="94"/>
        <source>Apply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-edit-account-widget.cpp" line="97"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-edit-account-widget.cpp" line="100"/>
        <source>Delete account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-edit-account-widget.cpp" line="123"/>
        <source>Gadu-Gadu number</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-edit-account-widget.cpp" line="128"/>
        <source>Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-edit-account-widget.cpp" line="130"/>
        <source>Remember password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-edit-account-widget.cpp" line="135"/>
        <source>Forgot Your Password?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-edit-account-widget.cpp" line="140"/>
        <source>Change Your Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-edit-account-widget.cpp" line="147"/>
        <source>Account Identity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-edit-account-widget.cpp" line="149"/>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Select or enter the identity that will be associated with this account.&lt;/i&gt;&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-edit-account-widget.cpp" line="158"/>
        <location filename="../gui/widgets/gadu-edit-account-widget.cpp" line="258"/>
        <source>General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-edit-account-widget.cpp" line="165"/>
        <source>Personal info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-edit-account-widget.cpp" line="176"/>
        <source>Buddies</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-edit-account-widget.cpp" line="182"/>
        <source>Connection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-edit-account-widget.cpp" line="187"/>
        <source>Proxy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-edit-account-widget.cpp" line="277"/>
        <source>Allow file transfers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-edit-account-widget.cpp" line="197"/>
        <source>Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-edit-account-widget.cpp" line="201"/>
        <source>Images</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-edit-account-widget.cpp" line="209"/>
        <source>Maximum image size that we want to receive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-edit-account-widget.cpp" line="212"/>
        <source>Maximum image size for chat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-edit-account-widget.cpp" line="214"/>
        <source>Receive images during invisibility</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-edit-account-widget.cpp" line="215"/>
        <source>Receiving images during invisibility is allowed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-edit-account-widget.cpp" line="224"/>
        <source>Define limit of images received per minute</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-edit-account-widget.cpp" line="227"/>
        <source>Limit numbers of image received per minute</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-edit-account-widget.cpp" line="231"/>
        <source>Status</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-edit-account-widget.cpp" line="234"/>
        <source>Show my status to everyone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-edit-account-widget.cpp" line="235"/>
        <source>When disabled, you&apos;re visible only to buddies on your list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-edit-account-widget.cpp" line="243"/>
        <source>Notifications</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-edit-account-widget.cpp" line="246"/>
        <source>Send composing events</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-edit-account-widget.cpp" line="264"/>
        <source>Use default servers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-edit-account-widget.cpp" line="267"/>
        <source>IP addresses</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-edit-account-widget.cpp" line="288"/>
        <source>Use encrypted connection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-edit-account-widget.cpp" line="444"/>
        <source>Confirm account removal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-edit-account-widget.cpp" line="445"/>
        <source>Are you sure do you want to remove account %1 (%2)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-edit-account-widget.cpp" line="449"/>
        <source>Remove account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-edit-account-widget.cpp" line="450"/>
        <source>Remove account and unregister from server</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GaduFormatter</name>
    <message>
        <location filename="../helpers/gadu-formatter.cpp" line="160"/>
        <source>###IMAGE BLOCKED###</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../helpers/gadu-formatter.cpp" line="170"/>
        <source>###IMAGE TOO BIG###</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GaduPersonalInfoWidget</name>
    <message>
        <location filename="../gui/widgets/gadu-personal-info-widget.cpp" line="70"/>
        <source>Unknown Gender</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-personal-info-widget.cpp" line="71"/>
        <source>Male</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-personal-info-widget.cpp" line="72"/>
        <source>Female</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-personal-info-widget.cpp" line="87"/>
        <source>Nick</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-personal-info-widget.cpp" line="88"/>
        <source>First name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-personal-info-widget.cpp" line="89"/>
        <source>Last name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-personal-info-widget.cpp" line="90"/>
        <source>Sex</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-personal-info-widget.cpp" line="91"/>
        <source>Family name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-personal-info-widget.cpp" line="92"/>
        <source>Birth year</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-personal-info-widget.cpp" line="93"/>
        <source>City</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/gadu-personal-info-widget.cpp" line="94"/>
        <source>Family city</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GaduProtocol</name>
    <message>
        <location filename="../gadu-protocol.cpp" line="399"/>
        <location filename="../gadu-protocol.cpp" line="679"/>
        <source>Kadu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gadu-protocol.cpp" line="399"/>
        <source>UIN not set!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gadu-protocol.cpp" line="408"/>
        <source>Please provide password for %1 (%2) account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gadu-protocol.cpp" line="668"/>
        <source>Unable to connect, server has not been found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gadu-protocol.cpp" line="672"/>
        <source>Unable to connect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gadu-protocol.cpp" line="676"/>
        <source>Please change your email in &quot;Change password / email&quot; window. Leave new password field blank.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gadu-protocol.cpp" line="683"/>
        <source>Unable to connect, server has returned unknown data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gadu-protocol.cpp" line="687"/>
        <source>Unable to connect, connection break during reading</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gadu-protocol.cpp" line="691"/>
        <source>Unable to connect, connection break during writing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gadu-protocol.cpp" line="701"/>
        <source>Unable to connect, error of negotiation TLS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gadu-protocol.cpp" line="705"/>
        <source>Too many connection attempts with bad password!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gadu-protocol.cpp" line="710"/>
        <source>Unable to connect, servers are down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gadu-protocol.cpp" line="714"/>
        <location filename="../gadu-protocol.cpp" line="728"/>
        <source>Connection broken</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gadu-protocol.cpp" line="719"/>
        <source>Connection timeout!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gadu-protocol.cpp" line="723"/>
        <source>Disconnection has occurred</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GaduProtocolFactory</name>
    <message>
        <location filename="../gadu-protocol-factory.cpp" line="111"/>
        <source>Gadu-Gadu number:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GaduRemindPasswordWindow</name>
    <message>
        <location filename="../gui/windows/gadu-remind-password-window.cpp" line="43"/>
        <source>Remind password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/windows/gadu-remind-password-window.cpp" line="62"/>
        <source>This dialog box allows you to ask server to remind your current password.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/windows/gadu-remind-password-window.cpp" line="76"/>
        <source>E-Mail Address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/windows/gadu-remind-password-window.cpp" line="78"/>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Type E-Mail Address used during registration.&lt;/i&gt;&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/windows/gadu-remind-password-window.cpp" line="86"/>
        <source>Characters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/windows/gadu-remind-password-window.cpp" line="88"/>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;For verification purposes, please type the characters above.&lt;/i&gt;&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/windows/gadu-remind-password-window.cpp" line="99"/>
        <source>Send Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/windows/gadu-remind-password-window.cpp" line="100"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/windows/gadu-remind-password-window.cpp" line="139"/>
        <location filename="../gui/windows/gadu-remind-password-window.cpp" line="143"/>
        <source>Kadu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/windows/gadu-remind-password-window.cpp" line="139"/>
        <source>Your password has been send on your email</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/windows/gadu-remind-password-window.cpp" line="143"/>
        <source>Error during remind password</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GaduUnregisterAccountWindow</name>
    <message>
        <location filename="../gui/windows/gadu-unregister-account-window.cpp" line="44"/>
        <source>Unregister account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/windows/gadu-unregister-account-window.cpp" line="62"/>
        <source>This dialog box allows you to unregister your account. Be aware of using this option.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/windows/gadu-unregister-account-window.cpp" line="69"/>
        <source>&lt;font size=&quot;+1&quot;&gt;&lt;b&gt;It will permanently delete your account and you will not be able to use it later!&lt;/b&gt;&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/windows/gadu-unregister-account-window.cpp" line="84"/>
        <source>Gadu-Gadu number</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/windows/gadu-unregister-account-window.cpp" line="89"/>
        <source>Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/windows/gadu-unregister-account-window.cpp" line="91"/>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Enter password for your Gadu-Gadu account.&lt;/i&gt;&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/windows/gadu-unregister-account-window.cpp" line="99"/>
        <source>Characters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/windows/gadu-unregister-account-window.cpp" line="101"/>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;For verification purposes, please type the characters above.&lt;/i&gt;&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/windows/gadu-unregister-account-window.cpp" line="112"/>
        <source>Unregister Account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/windows/gadu-unregister-account-window.cpp" line="113"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/windows/gadu-unregister-account-window.cpp" line="153"/>
        <location filename="../gui/windows/gadu-unregister-account-window.cpp" line="159"/>
        <source>Kadu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/windows/gadu-unregister-account-window.cpp" line="153"/>
        <source>Unregistation was successful. Now you don&apos;t have any GG number :(</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/windows/gadu-unregister-account-window.cpp" line="159"/>
        <source>An error has occurred while unregistration. Please try again later.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GaduWaitForAccountRegisterWindow</name>
    <message>
        <location filename="../gui/windows/gadu-wait-for-account-register-window.cpp" line="43"/>
        <source>Plase wait. New Gadu-Gadu account is being registered.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/windows/gadu-wait-for-account-register-window.cpp" line="55"/>
        <source>Registration was successful. Your new number is %1.
Store it in a safe place along with the password.
Now add your friends to the userlist.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/windows/gadu-wait-for-account-register-window.cpp" line="62"/>
        <source>An error has occurred during registration. Please try again later.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
