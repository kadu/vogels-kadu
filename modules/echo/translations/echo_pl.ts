<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="pl">
<context>
    <name>Echo</name>
    <message>
        <location filename="../echo.cpp" line="58"/>
        <location filename="../echo.cpp" line="67"/>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <location filename="../echo.cpp" line="58"/>
        <source>Echo started</source>
        <translation>Echo zostało uruchomione</translation>
    </message>
    <message>
        <location filename="../echo.cpp" line="67"/>
        <source>Echo stopped</source>
        <translation>Echo zostało zatrzymane</translation>
    </message>
</context>
</TS>
