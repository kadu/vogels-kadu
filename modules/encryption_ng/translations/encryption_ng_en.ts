<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="en_US">
<context>
    <name>@default</name>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="4"/>
        <source>Chat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="7"/>
        <source>Encryption</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="10"/>
        <source>General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="13"/>
        <source>Encrypt by default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="16"/>
        <source>Encrypt after receive encrypted message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="19"/>
        <source>Generate keys</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EncryptionActions</name>
    <message>
        <location filename="../encryption-actions.cpp" line="92"/>
        <source>Encrypt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../encryption-actions.cpp" line="99"/>
        <source>Send My Public Key</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../encryption-actions.cpp" line="166"/>
        <source>No public key available</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EncryptionNgConfigurationUiHandler</name>
    <message>
        <location filename="../encryption-ng-configuration-ui-handler.cpp" line="103"/>
        <location filename="../encryption-ng-configuration-ui-handler.cpp" line="108"/>
        <location filename="../encryption-ng-configuration-ui-handler.cpp" line="112"/>
        <location filename="../encryption-ng-configuration-ui-handler.cpp" line="114"/>
        <source>Encryption</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../encryption-ng-configuration-ui-handler.cpp" line="103"/>
        <source>Cannot generate keys. Check if encryption_simlite module is loaded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../encryption-ng-configuration-ui-handler.cpp" line="108"/>
        <source>Keys exist. Do you want to overwrite them?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../encryption-ng-configuration-ui-handler.cpp" line="112"/>
        <source>Keys have been generated</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../encryption-ng-configuration-ui-handler.cpp" line="114"/>
        <source>Error generating keys</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EncryptionNgNotification</name>
    <message>
        <location filename="../notify/encryption-ng-notification.cpp" line="92"/>
        <location filename="../notify/encryption-ng-notification.cpp" line="100"/>
        <location filename="../notify/encryption-ng-notification.cpp" line="108"/>
        <source>Encryption</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../notify/encryption-ng-notification.cpp" line="93"/>
        <source>Public key has been send to: %1 (%2)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../notify/encryption-ng-notification.cpp" line="101"/>
        <source>Error &quot;%3&quot; during sending public key to: %1 (%2)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../notify/encryption-ng-notification.cpp" line="109"/>
        <source>Error occured during encryption: %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EncryptionProviderManager</name>
    <message>
        <location filename="../encryption-provider-manager.cpp" line="157"/>
        <source>Buddy %1 is sending you his public key.
Do you want to save it?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../encryption-provider-manager.cpp" line="158"/>
        <source>Encryption</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="../encryption-ng-module.cpp" line="53"/>
        <source>Encryption</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../encryption-ng-module.cpp" line="54"/>
        <source>The QCA OSSL plugin for libqca2 is not present!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
