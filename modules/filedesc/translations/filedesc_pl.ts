<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="pl">
<context>
    <name>@default</name>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="4"/>
        <source>Status</source>
        <translation>Status</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="7"/>
        <location filename="../.configuration-ui-translations.cpp" line="10"/>
        <source>FileDesc</source>
        <translation>FileDesc</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="13"/>
        <source>File with description to synchronize</source>
        <translation>Plik z opisem do synchronizacji</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="15"/>
        <source>Enter here a file, which will contain descriptions to refresh by module.</source>
        <translation>Wpisz tutaj nazwę pliku, który będzie zawierał opisy do odświeżania przez moduł.</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="18"/>
        <source>Force status to be &apos;with description&apos;</source>
        <translation>Zmuszaj status, aby był &apos;z opisem&apos;</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="20"/>
        <source>If you choose status without description, module will set it automatically to similar but with description</source>
        <translation>Jeśli wybierzesz status bez opisu, moduł automatycznie ustawi analogiczny, ale z opisem</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="23"/>
        <source>Allow other descriptions</source>
        <translation>Dopuść inne opisy</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="25"/>
        <source>Allows you to set some custom description manualy, until file contents doesn&apos;t change</source>
        <translation>Pozwala na ustawienie dowolnego opisu ręcznie, dopóki zawartość pliku nie ulegnie zmianie</translation>
    </message>
</context>
</TS>
