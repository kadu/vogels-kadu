<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="pl">
<context>
    <name>@default</name>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="4"/>
        <source>MediaPlayer</source>
        <translation>MediaPlayer</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="7"/>
        <source>General</source>
        <translation>Ogólne</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="10"/>
        <source>MPRIS Media Player</source>
        <translation>Odtwarzacz zgodny ze standardem MPRIS</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="13"/>
        <source>Service</source>
        <translation>Usługa</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="15"/>
        <source>Usually: org.mpris.&lt;player name&gt; (eg. org.mpris.audacious) but for amarok is org.kde.amarok</source>
        <translation>Zazwyczaj: org.mpris.&lt;nazwa odtwarzacza&gt; (np. org.mpris.audacious) ale dla amaroka org.kde.amarok</translation>
    </message>
</context>
</TS>
