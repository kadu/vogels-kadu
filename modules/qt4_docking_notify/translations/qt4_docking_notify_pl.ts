<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="pl">
<context>
    <name>@default</name>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="4"/>
        <source>Notify</source>
        <translation>Powiadom</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="7"/>
        <source>Notification</source>
        <translation>Powiadomienia</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="10"/>
        <source>Tray Icon Balloon</source>
        <translation>Balonik ikony w zasobniku</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="13"/>
        <source>Timeout</source>
        <translation>Czas zaniku</translation>
    </message>
    <message numerus="yes">
        <location filename="../.configuration-ui-translations.cpp" line="15"/>
        <source>%n second(s)</source>
        <translation>
            <numerusform>%n sekunda</numerusform>
            <numerusform>%n sekundy</numerusform>
            <numerusform>%n sekund</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="18"/>
        <source>Notification icon</source>
        <translation>Ikona powiadomienia</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="21"/>
        <source>No icon</source>
        <translation>Bez ikony</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="24"/>
        <source>Information</source>
        <translation>Informacja</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="27"/>
        <source>Warning</source>
        <translation>Ostrzeżenie</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="30"/>
        <source>Critical</source>
        <translation>Błąd</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="34"/>
        <source>Title</source>
        <translation>Tytuł</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="37"/>
        <source>Syntax</source>
        <translation>Treść</translation>
    </message>
</context>
<context>
    <name>Qt4NotifyConfigurationWidget</name>
    <message>
        <location filename="../qt4_docking_notify_configuration_widget.cpp" line="44"/>
        <source>Configure</source>
        <translation>Konfiguruj</translation>
    </message>
    <message>
        <location filename="../qt4_docking_notify_configuration_widget.cpp" line="67"/>
        <source>Tray icon balloon&apos;s look configuration</source>
        <translation>Konfiguracja wyglądu balonika nad ikoną w zasobniku</translation>
    </message>
    <message>
        <location filename="../qt4_docking_notify_configuration_widget.cpp" line="74"/>
        <source>
%&amp;t - title (eg. New message) %&amp;m - notification text (eg. Message from Jim), %&amp;d - details (eg. message quotation),
%&amp;i - notification icon</source>
        <translation>%&amp;t - tytuł (np. Nowa wiadomość) %&amp;m - treść powiadomienia (np. Wiadomość od Jasia), %&amp;d - szczegóły (np. treść wiadomości),%&amp;i - ikona powiadomienia</translation>
    </message>
</context>
</TS>
