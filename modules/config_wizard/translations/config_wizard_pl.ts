<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="pl">
<context>
    <name>@default</name>
    <message>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Ogólne</translation>
    </message>
    <message>
        <source>Config Wizard</source>
        <translation>Kreator konfiguracji</translation>
    </message>
    <message>
		<source>Start Configuration Wizard...</source>
		<translation>Uruchom kreator konfiguracji...</translation>
    </message>
</context>
<context>
    <name>ConfigWizardChooseNetworkPage</name>
    <message>
        <source>&lt;h3&gt;Account Setup&lt;/h3&gt;</source>
        <translation>&lt;h3&gt;Tworzenie konta&lt;/h3&gt;</translation>
    </message>
    <message>
        <source>IM Network</source>
        <translation>Sieć</translation>
    </message>
    <message>
        <source>I want to set up existing account for Kadu</source>
        <translation>Chcę dodać istniejące konto</translation>
    </message>
    <message>
        <source>I want to create new account for Kadu</source>
        <translation>Chcę zarejestrować nowe konto</translation>
    </message>
    <message>
        <source>I don&apos;t want to set up my account for Kadu now</source>
        <translation>Nie chcę teraz dodawać konta</translation>
    </message>
</context>
<context>
    <name>ConfigWizardCompletedPage</name>
    <message>
        <source>&lt;h3&gt;Configuration Wizard Completed&lt;/h3&gt;</source>
        <translation>&lt;h3&gt;Konfiguracja zakończona&lt;/h3&gt;</translation>
    </message>
    <message>
        <source>Go to Accounts Setting after closing this window</source>
        <translation>Przejdź do okna konfiguracji kont po zamknięciu tego okna</translation>
    </message>
</context>
<context>
    <name>ConfigWizardProfilePage</name>
    <message>
        <source>&lt;h3&gt;Welcome to Kadu Instant Messenger&lt;/h3&gt;&lt;p&gt;This wizard will help you to configure the basic settings of Kadu.&lt;/p&gt;&lt;p&gt;Please choose a preferred language and create a nickname&lt;/p&gt;</source>
        <translation>&lt;h3&gt;Witamy w Kadu&lt;/h3&gt;&lt;p&gt;Ten kreator pomoże ci ustawić podstawową konfigurację programu.&lt;/p&gt;&lt;p&gt;Proszę wybrać język i podać swój pseudonim/imię</translation>
    </message>
    <message>
        <source>&lt;h3&gt;Profile setup&lt;/h3&gt;</source>
        <translation>&lt;h3&gt;Ustawienia profilu&lt;/h3&gt;</translation>
    </message>
    <message>
        <source>Language</source>
        <translation>Język</translation>
    </message>
    <message>
        <source>Nickname</source>
        <translation>Nick</translation>
    </message>
</context>
<context>
    <name>ConfigWizardSetUpAccountPage</name>
    <message>
        <source>&lt;h3&gt;Account Setup&lt;/h3&gt;</source>
        <translation>&lt;h3&gt;Tworzenie konta&lt;/h3&gt;</translation>
    </message>
</context>
<context>
    <name>ConfigWizardWindow</name>
    <message>
        <source>Kadu Wizard</source>
        <translation>Kreator konfiguracji</translation>
    </message>
</context>
</TS>
