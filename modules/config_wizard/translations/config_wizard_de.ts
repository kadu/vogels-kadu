<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="de">
<context>
    <name>@default</name>
    <message>
        <source>Kadu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Config Wizard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
		<source>Start Configuration Wizard...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ConfigWizardChooseNetworkPage</name>
    <message>
        <source>&lt;h3&gt;Account Setup&lt;/h3&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>IM Network</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>I want to set up existing account for Kadu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>I want to create new account for Kadu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>I don&apos;t want to set up my account for Kadu now</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ConfigWizardCompletedPage</name>
    <message>
        <source>&lt;h3&gt;Configuration Wizard Completed&lt;/h3&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Go to Accounts Setting after closing this window</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ConfigWizardProfilePage</name>
    <message>
        <source>&lt;h3&gt;Welcome to Kadu Instant Messenger&lt;/h3&gt;&lt;p&gt;This wizard will help you to configure the basic settings of Kadu.&lt;/p&gt;&lt;p&gt;Please choose a preferred language and create a nickname&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;h3&gt;Profile setup&lt;/h3&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Nickname</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ConfigWizardSetUpAccountPage</name>
    <message>
        <source>&lt;h3&gt;Account Setup&lt;/h3&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ConfigWizardWindow</name>
    <message>
        <source>Kadu Wizard</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
