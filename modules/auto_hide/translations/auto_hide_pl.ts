<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="pl">
<context>
    <name>@default</name>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="4"/>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="7"/>
        <source>Advanced</source>
        <translation>Zaawansowane</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="10"/>
        <source>Kadu&apos;s Main Window Autohide</source>
        <translation>Autoukrywanie głownego okna Kadu</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="13"/>
        <source>Use autohide</source>
        <translation>Włącz autoukrywanie</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="16"/>
        <source>Autohide idle time</source>
        <translation>Czas bezczynności</translation>
    </message>
    <message numerus="yes">
        <location filename="../.configuration-ui-translations.cpp" line="18"/>
        <source>%n second(s)</source>
        <translation>
            <numerusform>%n sekunda</numerusform>
            <numerusform>%n sekundy</numerusform>
            <numerusform>%n sekund</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>AutoHide</name>
    <message>
        <location filename="../auto_hide.cpp" line="114"/>
        <source>Don&apos;t hide</source>
        <translation>Nie ukrywaj</translation>
    </message>
</context>
</TS>
