<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="de">
<context>
    <name>@default</name>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="4"/>
        <source>Kadu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="7"/>
        <source>Advanced</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="10"/>
        <source>Kadu&apos;s Main Window Autohide</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="13"/>
        <source>Use autohide</source>
        <translation>Automatisches Verstecken einschalten</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="16"/>
        <source>Autohide idle time</source>
        <translation>Inaktivitätsdauer</translation>
    </message>
    <message numerus="yes">
        <location filename="../.configuration-ui-translations.cpp" line="18"/>
        <source>%n second(s)</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
</context>
<context>
    <name>AutoHide</name>
    <message>
        <location filename="../auto_hide.cpp" line="114"/>
        <source>Don&apos;t hide</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
