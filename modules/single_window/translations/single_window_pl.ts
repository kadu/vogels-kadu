<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="pl">
<context>
    <name>@default</name>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="4"/>
        <source>Look</source>
        <translation>Wygląd</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="7"/>
        <location filename="../.configuration-ui-translations.cpp" line="34"/>
        <source>SingleWindow</source>
        <translation>Pojedyncze okno</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="10"/>
        <source>General</source>
        <translation>Ogólne</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="13"/>
        <source>Roster position</source>
        <translation>Pozycja listy kontaktów</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="15"/>
        <source>Choose position of roster in Single Window mode</source>
        <translation>Wybierz pozycję listy kontaktów w trybie pojedynczego okna</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="18"/>
        <source>Left</source>
        <translation>Strona lewa</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="21"/>
        <source>Right</source>
        <translation>Strona prawa</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="25"/>
        <source>Show number of messages on tab</source>
        <translation>Wyświetl liczbę wiadomości na zakładce</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="31"/>
        <source>Shortcuts</source>
        <translation>Skróty</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="37"/>
        <source>Tabs</source>
        <translation>Zakładki</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="40"/>
        <source>Switch to previous tab</source>
        <translation>Przełącz na poprzednią zakładkę</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="43"/>
        <source>Switch to next tab</source>
        <translation>Przełącz na następną zakładkę</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="46"/>
        <source>Show/hide roster</source>
        <translation>Pokaż/ukryj listę kontaktów</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="49"/>
        <source>Switch focus between roster and tabs</source>
        <translation>Przełącz focus pomiędzy listą kontaktów a zakładkami</translation>
    </message>
</context>
</TS>
