<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="en_US">
<context>
    <name>CertificateDisplayDialog</name>
    <message>
        <location filename="../certificates/certificate-display-dialog.cpp" line="130"/>
        <source>Certificate Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../certificates/certificate-display-dialog.cpp" line="131"/>
        <source>Certificate Validation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../certificates/certificate-display-dialog.cpp" line="133"/>
        <source>Valid From</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../certificates/certificate-display-dialog.cpp" line="135"/>
        <source>Valid Until</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../certificates/certificate-display-dialog.cpp" line="137"/>
        <source>Serial Number</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../certificates/certificate-display-dialog.cpp" line="139"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../certificates/certificate-display-dialog.cpp" line="153"/>
        <source>The certificate is valid.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../certificates/certificate-display-dialog.cpp" line="158"/>
        <source>The certificate is NOT valid!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../certificates/certificate-display-dialog.cpp" line="158"/>
        <source>Reason: %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../certificates/certificate-display-dialog.cpp" line="174"/>
        <source>Subject Details:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../certificates/certificate-display-dialog.cpp" line="175"/>
        <source>Issuer Details:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../certificates/certificate-display-dialog.cpp" line="190"/>
        <source>Organization:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../certificates/certificate-display-dialog.cpp" line="191"/>
        <source>Organizational unit:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../certificates/certificate-display-dialog.cpp" line="192"/>
        <source>Locality:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../certificates/certificate-display-dialog.cpp" line="193"/>
        <source>State:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../certificates/certificate-display-dialog.cpp" line="194"/>
        <source>Country:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../certificates/certificate-display-dialog.cpp" line="195"/>
        <source>Common name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../certificates/certificate-display-dialog.cpp" line="196"/>
        <source>Domain name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../certificates/certificate-display-dialog.cpp" line="197"/>
        <source>XMPP name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../certificates/certificate-display-dialog.cpp" line="198"/>
        <source>Email:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FacebookProtocolFactory</name>
    <message>
        <location filename="../facebook-protocol-factory.cpp" line="76"/>
        <source>Facebook ID:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../facebook-protocol-factory.cpp" line="91"/>
        <source>Your username is available at &lt;a href=&apos;https://www.facebook.com/editaccount.php?settings&apos;&gt;https://www.facebook.com/editaccount.php?settings&lt;/a&gt; under Username field. If this field is empty, you can choose your Username and enter it there.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GTalkProtocolFactory</name>
    <message>
        <location filename="../gtalk-protocol-factory.cpp" line="68"/>
        <source>Gmail/Google Talk ID:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HttpConnect</name>
    <message>
        <location filename="../libiris/src/irisnet/noncore/cutestuff/httpconnect.cpp" line="320"/>
        <source>Authentication failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/src/irisnet/noncore/cutestuff/httpconnect.cpp" line="324"/>
        <source>Host not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/src/irisnet/noncore/cutestuff/httpconnect.cpp" line="328"/>
        <source>Access denied</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/src/irisnet/noncore/cutestuff/httpconnect.cpp" line="332"/>
        <source>Connection refused</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/src/irisnet/noncore/cutestuff/httpconnect.cpp" line="336"/>
        <source>Invalid reply</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HttpProxyGetStream</name>
    <message>
        <location filename="../libiris/src/irisnet/noncore/cutestuff/httppoll.cpp" line="909"/>
        <source>Authentication failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/src/irisnet/noncore/cutestuff/httppoll.cpp" line="913"/>
        <source>Host not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/src/irisnet/noncore/cutestuff/httppoll.cpp" line="917"/>
        <source>Access denied</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/src/irisnet/noncore/cutestuff/httppoll.cpp" line="921"/>
        <source>Connection refused</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/src/irisnet/noncore/cutestuff/httppoll.cpp" line="925"/>
        <source>Invalid reply</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HttpProxyPost</name>
    <message>
        <location filename="../libiris/src/irisnet/noncore/cutestuff/httppoll.cpp" line="626"/>
        <source>Authentication failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/src/irisnet/noncore/cutestuff/httppoll.cpp" line="630"/>
        <source>Host not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/src/irisnet/noncore/cutestuff/httppoll.cpp" line="634"/>
        <source>Access denied</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/src/irisnet/noncore/cutestuff/httppoll.cpp" line="638"/>
        <source>Connection refused</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/src/irisnet/noncore/cutestuff/httppoll.cpp" line="642"/>
        <source>Invalid reply</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>JabberActions</name>
    <message>
        <location filename="../actions/jabber-actions.cpp" line="72"/>
        <source>Show XML Console for Account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../actions/jabber-actions.cpp" line="89"/>
        <source>Resend Subscription</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../actions/jabber-actions.cpp" line="92"/>
        <source>Remove Subscription</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../actions/jabber-actions.cpp" line="95"/>
        <source>Ask for Subscription</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>JabberAddAccountWidget</name>
    <message>
        <location filename="../gui/widgets/jabber-add-account-widget.cpp" line="93"/>
        <source>&lt;a href=&apos;#&apos;&gt;What is my username?&lt;/a&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/jabber-add-account-widget.cpp" line="107"/>
        <source>Username</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/jabber-add-account-widget.cpp" line="112"/>
        <source>Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/jabber-add-account-widget.cpp" line="114"/>
        <source>Remember Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/jabber-add-account-widget.cpp" line="119"/>
        <source>Account Identity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/jabber-add-account-widget.cpp" line="121"/>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Select or enter the identity that will be associated with this account.&lt;/i&gt;&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/jabber-add-account-widget.cpp" line="132"/>
        <source>Add Account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/jabber-add-account-widget.cpp" line="133"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>JabberChangePasswordWindow</name>
    <message>
        <location filename="../gui/windows/jabber-change-password-window.cpp" line="41"/>
        <location filename="../gui/windows/jabber-change-password-window.cpp" line="102"/>
        <source>Change Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/windows/jabber-change-password-window.cpp" line="59"/>
        <source>This dialog box allows you to change your current password.
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/windows/jabber-change-password-window.cpp" line="73"/>
        <source>Old Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/windows/jabber-change-password-window.cpp" line="75"/>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Enter current password for your XMPP/Jabber account.&lt;/i&gt;&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/windows/jabber-change-password-window.cpp" line="84"/>
        <source>New password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/windows/jabber-change-password-window.cpp" line="86"/>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Enter new password for your XMPP/Jabber account.&lt;/i&gt;&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/windows/jabber-change-password-window.cpp" line="95"/>
        <source>Retype new password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/windows/jabber-change-password-window.cpp" line="103"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/windows/jabber-change-password-window.cpp" line="126"/>
        <location filename="../gui/windows/jabber-change-password-window.cpp" line="151"/>
        <location filename="../gui/windows/jabber-change-password-window.cpp" line="159"/>
        <source>Kadu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/windows/jabber-change-password-window.cpp" line="126"/>
        <source>Invalid data entered in required fields.

Password entered in both fields (&quot;Password&quot; and &quot;Retype password&quot;) should be the same!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/windows/jabber-change-password-window.cpp" line="151"/>
        <source>Changing password was successful.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/windows/jabber-change-password-window.cpp" line="159"/>
        <source>An error has occurred. Please try again later.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>JabberContactPersonalInfoWidget</name>
    <message>
        <location filename="../gui/widgets/jabber-contact-personal-info-widget.cpp" line="76"/>
        <source>Full Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/jabber-contact-personal-info-widget.cpp" line="79"/>
        <source>Family Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/jabber-contact-personal-info-widget.cpp" line="82"/>
        <source>Nickname</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/jabber-contact-personal-info-widget.cpp" line="85"/>
        <source>Birthdate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/jabber-contact-personal-info-widget.cpp" line="88"/>
        <source>City</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/jabber-contact-personal-info-widget.cpp" line="91"/>
        <source>E-Mail</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/jabber-contact-personal-info-widget.cpp" line="95"/>
        <source>Website</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>JabberCreateAccountWidget</name>
    <message>
        <location filename="../gui/widgets/jabber-create-account-widget.cpp" line="95"/>
        <source>Username</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/jabber-create-account-widget.cpp" line="100"/>
        <source>Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/jabber-create-account-widget.cpp" line="105"/>
        <source>Retype Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/jabber-create-account-widget.cpp" line="107"/>
        <source>Remember password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/jabber-create-account-widget.cpp" line="112"/>
        <source>Account Identity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/jabber-create-account-widget.cpp" line="114"/>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Select or enter the identity that will be associated with this account.&lt;/i&gt;&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/jabber-create-account-widget.cpp" line="121"/>
        <source>More options:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/jabber-create-account-widget.cpp" line="141"/>
        <source>Connection settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/jabber-create-account-widget.cpp" line="148"/>
        <source>Manually Specify Server Host/Port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/jabber-create-account-widget.cpp" line="157"/>
        <source>Host</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/jabber-create-account-widget.cpp" line="164"/>
        <source>Port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/jabber-create-account-widget.cpp" line="179"/>
        <source>Encrypt connection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/jabber-create-account-widget.cpp" line="183"/>
        <source>Always</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/jabber-create-account-widget.cpp" line="184"/>
        <source>When available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/jabber-create-account-widget.cpp" line="185"/>
        <source>Legacy SSL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/jabber-create-account-widget.cpp" line="194"/>
        <source>Probe legacy SSL port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/jabber-create-account-widget.cpp" line="204"/>
        <source>Register Account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/jabber-create-account-widget.cpp" line="205"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/jabber-create-account-widget.cpp" line="221"/>
        <location filename="../gui/widgets/jabber-create-account-widget.cpp" line="243"/>
        <location filename="../gui/widgets/jabber-create-account-widget.cpp" line="285"/>
        <source>Kadu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/jabber-create-account-widget.cpp" line="221"/>
        <source>Cannot enable secure connection. SSL/TLS plugin not found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/jabber-create-account-widget.cpp" line="243"/>
        <source>Legacy secure connection (SSL) is only available in combination with manual host/port.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/jabber-create-account-widget.cpp" line="285"/>
        <source>Invalid data entered in required fields.

Password entered in both fields (&quot;New password&quot; and &quot;Retype password&quot;) should be the same!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>JabberEditAccountWidget</name>
    <message>
        <location filename="../gui/widgets/jabber-edit-account-widget.cpp" line="83"/>
        <source>Apply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/jabber-edit-account-widget.cpp" line="86"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/jabber-edit-account-widget.cpp" line="89"/>
        <source>Delete account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/jabber-edit-account-widget.cpp" line="112"/>
        <source>Username</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/jabber-edit-account-widget.cpp" line="117"/>
        <source>Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/jabber-edit-account-widget.cpp" line="119"/>
        <source>Remember password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/jabber-edit-account-widget.cpp" line="124"/>
        <source>Change your password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/jabber-edit-account-widget.cpp" line="131"/>
        <source>Account Identity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/jabber-edit-account-widget.cpp" line="133"/>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Select or enter the identity that will be associated with this account.&lt;/i&gt;&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/jabber-edit-account-widget.cpp" line="142"/>
        <location filename="../gui/widgets/jabber-edit-account-widget.cpp" line="170"/>
        <source>General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/jabber-edit-account-widget.cpp" line="149"/>
        <source>Personal Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/jabber-edit-account-widget.cpp" line="155"/>
        <source>Connection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/jabber-edit-account-widget.cpp" line="160"/>
        <source>Proxy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/jabber-edit-account-widget.cpp" line="178"/>
        <source>Manually specify server host/port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/jabber-edit-account-widget.cpp" line="186"/>
        <source>Host</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/jabber-edit-account-widget.cpp" line="194"/>
        <source>Port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/jabber-edit-account-widget.cpp" line="218"/>
        <source>Encrypt connection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/jabber-edit-account-widget.cpp" line="222"/>
        <location filename="../gui/widgets/jabber-edit-account-widget.cpp" line="247"/>
        <source>Never</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/jabber-edit-account-widget.cpp" line="223"/>
        <location filename="../gui/widgets/jabber-edit-account-widget.cpp" line="248"/>
        <source>Always</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/jabber-edit-account-widget.cpp" line="224"/>
        <source>When available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/jabber-edit-account-widget.cpp" line="225"/>
        <source>Legacy SSL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/jabber-edit-account-widget.cpp" line="235"/>
        <source>Probe legacy SSL port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/jabber-edit-account-widget.cpp" line="243"/>
        <source>Allow plaintext authentication</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/jabber-edit-account-widget.cpp" line="249"/>
        <source>Over encrypted connection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/jabber-edit-account-widget.cpp" line="259"/>
        <source>Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/jabber-edit-account-widget.cpp" line="265"/>
        <location filename="../gui/widgets/jabber-edit-account-widget.cpp" line="278"/>
        <source>Resource</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/jabber-edit-account-widget.cpp" line="272"/>
        <source>Use hostname as a resource</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/jabber-edit-account-widget.cpp" line="286"/>
        <source>Priority</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/jabber-edit-account-widget.cpp" line="297"/>
        <location filename="../gui/widgets/jabber-edit-account-widget.cpp" line="304"/>
        <source>Data transfer proxy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/jabber-edit-account-widget.cpp" line="312"/>
        <source>Notifications</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/jabber-edit-account-widget.cpp" line="315"/>
        <source>Send composing events</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/jabber-edit-account-widget.cpp" line="319"/>
        <source>Send inactivity events (end/suspend conversation)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/jabber-edit-account-widget.cpp" line="351"/>
        <location filename="../gui/widgets/jabber-edit-account-widget.cpp" line="363"/>
        <source>Kadu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/jabber-edit-account-widget.cpp" line="351"/>
        <source>Cannot enable secure connection. SSL/TLS plugin not found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/jabber-edit-account-widget.cpp" line="363"/>
        <source>Legacy SSL is only available in combination with manual host/port.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/jabber-edit-account-widget.cpp" line="504"/>
        <source>Confirm account removal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/jabber-edit-account-widget.cpp" line="505"/>
        <source>Are you sure you want to remove account %1 (%2)?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/jabber-edit-account-widget.cpp" line="509"/>
        <source>Remove account</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>JabberPersonalInfoWidget</name>
    <message>
        <location filename="../gui/widgets/jabber-personal-info-widget.cpp" line="79"/>
        <source>Full name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/jabber-personal-info-widget.cpp" line="80"/>
        <source>Nick</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/jabber-personal-info-widget.cpp" line="81"/>
        <source>Family name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/jabber-personal-info-widget.cpp" line="82"/>
        <source>Birth year</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/jabber-personal-info-widget.cpp" line="83"/>
        <source>City</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/jabber-personal-info-widget.cpp" line="84"/>
        <source>E-Mail</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/jabber-personal-info-widget.cpp" line="85"/>
        <source>Website</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>JabberProtocol</name>
    <message>
        <location filename="../jabber-protocol.cpp" line="253"/>
        <source>Kadu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../jabber-protocol.cpp" line="253"/>
        <source>XMPP username is not set!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../jabber-protocol.cpp" line="262"/>
        <source>Please provide password for %1 (%2) account</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>JabberProtocolFactory</name>
    <message>
        <location filename="../jabber-protocol-factory.cpp" line="119"/>
        <source>User JID:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>JabberServerRegisterAccount</name>
    <message>
        <location filename="../server/jabber-server-register-account.cpp" line="88"/>
        <location filename="../server/jabber-server-register-account.cpp" line="147"/>
        <source>Kadu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../server/jabber-server-register-account.cpp" line="88"/>
        <source>This server does not support registration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../server/jabber-server-register-account.cpp" line="147"/>
        <source>There was an error registering the account.
Reason: %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>JabberSubscriptionService</name>
    <message>
        <location filename="../services/jabber-subscription-service.cpp" line="52"/>
        <source>Kadu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../services/jabber-subscription-service.cpp" line="52"/>
        <source>The user %1 removed subscription to you. You will no longer be able to view his/her online/offline status. Do you want to delete the contact?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../services/jabber-subscription-service.cpp" line="96"/>
        <source>Kadu - authorize user?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../services/jabber-subscription-service.cpp" line="96"/>
        <source>The user %1 (%2) is asking for subscription from you. He will be able to view your online/offline status. Do you want to authorize the contact?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>JabberWaitForAccountRegisterWindow</name>
    <message>
        <location filename="../gui/windows/jabber-wait-for-account-register-window.cpp" line="43"/>
        <source>Plase wait. New XMPP account is being registered.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/windows/jabber-wait-for-account-register-window.cpp" line="55"/>
        <source>Registration was successful. Your new XMPP username is %1.
Store it in a safe place along with the password.
Now please add your friends to the buddy list.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/windows/jabber-wait-for-account-register-window.cpp" line="63"/>
        <source>An error has occurred during registration. Please try again later.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MiniClient</name>
    <message>
        <location filename="../client/mini-client.cpp" line="171"/>
        <source>Server Authentication</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../client/mini-client.cpp" line="239"/>
        <location filename="../client/mini-client.cpp" line="253"/>
        <source>Server Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../client/mini-client.cpp" line="239"/>
        <source>The server does not support TLS encryption.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../client/mini-client.cpp" line="253"/>
        <source>There was an error communicating with the Jabber server.
Details: %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../certificates/certificate-error-dialog.cpp" line="42"/>
        <source>The %1 certificate failed the authenticity test.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../certificates/certificate-error-dialog.cpp" line="45"/>
        <source>&amp;Details...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../certificates/certificate-error-dialog.cpp" line="46"/>
        <source>&amp;Connect anyway</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../certificates/certificate-error-dialog.cpp" line="49"/>
        <source>&amp;Trust this certificate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../certificates/certificate-error-dialog.cpp" line="51"/>
        <source>&amp;Trust this domain</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../certificates/certificate-helpers.cpp" line="171"/>
        <source>The server did not present a certificate.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../certificates/certificate-helpers.cpp" line="174"/>
        <source>Certificate is valid.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../certificates/certificate-helpers.cpp" line="177"/>
        <source>The hostname does not match the one the certificate was issued to.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../certificates/certificate-helpers.cpp" line="184"/>
        <source>General certificate validation error.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/tools/xmpptest/xmpptest.cpp" line="86"/>
        <source>No certificate presented.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/tools/xmpptest/xmpptest.cpp" line="91"/>
        <source>Hostname mismatch.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/tools/xmpptest/xmpptest.cpp" line="94"/>
        <source>Invalid Certificate.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/tools/xmpptest/xmpptest.cpp" line="123"/>
        <source>General validation error.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/src/xmpp/xmpp-im/types.cpp" line="2764"/>
        <source>Username</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/src/xmpp/xmpp-im/types.cpp" line="2765"/>
        <source>Nickname</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/src/xmpp/xmpp-im/types.cpp" line="2766"/>
        <source>Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/src/xmpp/xmpp-im/types.cpp" line="2767"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/src/xmpp/xmpp-im/types.cpp" line="2768"/>
        <source>First Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/src/xmpp/xmpp-im/types.cpp" line="2769"/>
        <source>Last Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/src/xmpp/xmpp-im/types.cpp" line="2770"/>
        <source>E-mail</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/src/xmpp/xmpp-im/types.cpp" line="2771"/>
        <source>Address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/src/xmpp/xmpp-im/types.cpp" line="2772"/>
        <source>City</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/src/xmpp/xmpp-im/types.cpp" line="2773"/>
        <source>State</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/src/xmpp/xmpp-im/types.cpp" line="2774"/>
        <source>Zipcode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/src/xmpp/xmpp-im/types.cpp" line="2775"/>
        <source>Phone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/src/xmpp/xmpp-im/types.cpp" line="2776"/>
        <source>URL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/src/xmpp/xmpp-im/types.cpp" line="2777"/>
        <source>Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/src/xmpp/xmpp-im/types.cpp" line="2778"/>
        <source>Misc</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TestDlg</name>
    <message>
        <location filename="../libiris/tools/xmpptest/xmpptest.cpp" line="203"/>
        <source>XMPP Test</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/tools/xmpptest/xmpptest.cpp" line="223"/>
        <location filename="../libiris/tools/xmpptest/xmpptest.cpp" line="322"/>
        <source>&amp;Connect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/tools/xmpptest/xmpptest.cpp" line="290"/>
        <source>About %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/tools/xmpptest/xmpptest.cpp" line="290"/>
        <source>%1 v1.0

Utility to demonstrate the Iris XMPP library.

Currently supports:
  draft-ietf-xmpp-core-21
  JEP-0025

Copyright (C) 2003 Justin Karneges</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/tools/xmpptest/xmpptest.cpp" line="336"/>
        <location filename="../libiris/tools/xmpptest/xmpptest.cpp" line="346"/>
        <location filename="../libiris/tools/xmpptest/xmpptest.cpp" line="350"/>
        <location filename="../libiris/tools/xmpptest/xmpptest.cpp" line="358"/>
        <location filename="../libiris/tools/xmpptest/xmpptest.cpp" line="383"/>
        <location filename="../libiris/tools/xmpptest/xmpptest.cpp" line="393"/>
        <location filename="../libiris/tools/xmpptest/xmpptest.cpp" line="398"/>
        <location filename="../libiris/tools/xmpptest/xmpptest.cpp" line="478"/>
        <location filename="../libiris/tools/xmpptest/xmpptest.cpp" line="492"/>
        <location filename="../libiris/tools/xmpptest/xmpptest.cpp" line="500"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/tools/xmpptest/xmpptest.cpp" line="336"/>
        <source>Please enter the Full JID to connect with.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/tools/xmpptest/xmpptest.cpp" line="346"/>
        <source>You must specify a host:port for the proxy.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/tools/xmpptest/xmpptest.cpp" line="350"/>
        <source>You must at least enter a URL to use http poll.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/tools/xmpptest/xmpptest.cpp" line="358"/>
        <source>Please enter the proxy host in the form &apos;host:port&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/tools/xmpptest/xmpptest.cpp" line="383"/>
        <source>Please enter the host in the form &apos;host:port&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/tools/xmpptest/xmpptest.cpp" line="393"/>
        <source>Error: SSF Min is greater than SSF Max.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/tools/xmpptest/xmpptest.cpp" line="398"/>
        <source>Error: TLS not available.  Disable any TLS options.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/tools/xmpptest/xmpptest.cpp" line="423"/>
        <source>&amp;Disconnect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/tools/xmpptest/xmpptest.cpp" line="478"/>
        <source>Bad XML input (%1,%2): %3
Please correct and try again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/tools/xmpptest/xmpptest.cpp" line="492"/>
        <source>Bad Stanza &apos;%1&apos;.  Must be &apos;message&apos;, &apos;presence&apos;, or &apos;iq&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/tools/xmpptest/xmpptest.cpp" line="500"/>
        <source>You must enter at least one stanza!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/tools/xmpptest/xmpptest.cpp" line="655"/>
        <source>Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/tools/xmpptest/xmpptest.cpp" line="655"/>
        <source>Enter the password for %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>XMPP::ClientStream</name>
    <message>
        <location filename="../libiris/src/xmpp/xmpp-core/stream.cpp" line="782"/>
        <source>Offered mechanisms: </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>XMPP::Features::FeatureName</name>
    <message>
        <location filename="../libiris/src/xmpp/xmpp-im/xmpp_features.cpp" line="181"/>
        <source>ERROR: Incorrect usage of Features class</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/src/xmpp/xmpp-im/xmpp_features.cpp" line="182"/>
        <source>None</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/src/xmpp/xmpp-im/xmpp_features.cpp" line="183"/>
        <source>Register</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/src/xmpp/xmpp-im/xmpp_features.cpp" line="184"/>
        <source>Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/src/xmpp/xmpp-im/xmpp_features.cpp" line="185"/>
        <source>Groupchat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/src/xmpp/xmpp-im/xmpp_features.cpp" line="186"/>
        <source>Gateway</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/src/xmpp/xmpp-im/xmpp_features.cpp" line="187"/>
        <source>Service Discovery</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/src/xmpp/xmpp-im/xmpp_features.cpp" line="188"/>
        <source>VCard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/src/xmpp/xmpp-im/xmpp_features.cpp" line="189"/>
        <source>Execute command</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/src/xmpp/xmpp-im/xmpp_features.cpp" line="192"/>
        <source>Add to roster</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>XMPP::JT_VCard</name>
    <message>
        <location filename="../libiris/src/xmpp/xmpp-im/xmpp_tasks.cpp" line="1070"/>
        <source>No VCard available</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>XMPP::JabberClient</name>
    <message>
        <location filename="../client/jabber-client.cpp" line="252"/>
        <source>SSL support could not be initialized for account %1. This is most likely because the QCA TLS plugin is not installed on your system.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../client/jabber-client.cpp" line="557"/>
        <source>Server Authentication</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../client/jabber-client.cpp" line="655"/>
        <source>The server does not support TLS encryption.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../client/jabber-client.cpp" line="684"/>
        <source>There was an error communicating with the server.
Details: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../client/jabber-client.cpp" line="887"/>
        <source>Disconnected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../client/jabber-client.cpp" line="892"/>
        <source>XML Parsing Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../client/jabber-client.cpp" line="897"/>
        <source>XMPP Protocol Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../client/jabber-client.cpp" line="917"/>
        <source>Generic stream error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../client/jabber-client.cpp" line="920"/>
        <source>Conflict(remote login replacing this one)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../client/jabber-client.cpp" line="924"/>
        <source>Timed out from inactivity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../client/jabber-client.cpp" line="926"/>
        <source>Internal server error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../client/jabber-client.cpp" line="928"/>
        <source>Invalid XML</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../client/jabber-client.cpp" line="931"/>
        <source>Policy violation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../client/jabber-client.cpp" line="936"/>
        <source>Server out of resources</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../client/jabber-client.cpp" line="941"/>
        <source>Server is shutting down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../client/jabber-client.cpp" line="943"/>
        <source>XMPP Stream Error: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../client/jabber-client.cpp" line="951"/>
        <source>Unable to connect to server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../client/jabber-client.cpp" line="953"/>
        <source>Host not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../client/jabber-client.cpp" line="955"/>
        <source>Error connecting to proxy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../client/jabber-client.cpp" line="957"/>
        <source>Error during proxy negotiation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../client/jabber-client.cpp" line="960"/>
        <source>Proxy authentication failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../client/jabber-client.cpp" line="964"/>
        <source>Socket/stream error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../client/jabber-client.cpp" line="965"/>
        <source>Connection Error: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../client/jabber-client.cpp" line="973"/>
        <source>Host no longer hosted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../client/jabber-client.cpp" line="975"/>
        <source>Host unknown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../client/jabber-client.cpp" line="978"/>
        <source>A required remote connection failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../client/jabber-client.cpp" line="982"/>
        <source>See other host: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../client/jabber-client.cpp" line="984"/>
        <source>Server does not support proper XMPP version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../client/jabber-client.cpp" line="985"/>
        <source>Stream Negotiation Error: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../client/jabber-client.cpp" line="992"/>
        <source>Server rejected STARTTLS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../client/jabber-client.cpp" line="997"/>
        <source>TLS handshake error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../client/jabber-client.cpp" line="999"/>
        <source>Broken security layer (TLS)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../client/jabber-client.cpp" line="1008"/>
        <source>Unable to login</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../client/jabber-client.cpp" line="1011"/>
        <source>No appropriate mechanism available for given security settings(e.g. SASL library too weak, or plaintext authentication not enabled)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../client/jabber-client.cpp" line="1015"/>
        <source>Bad server response</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../client/jabber-client.cpp" line="1017"/>
        <source>Server failed mutual authentication</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../client/jabber-client.cpp" line="1019"/>
        <source>Encryption required for chosen SASL mechanism</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../client/jabber-client.cpp" line="1021"/>
        <source>Invalid account information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../client/jabber-client.cpp" line="1023"/>
        <source>Invalid SASL mechanism</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../client/jabber-client.cpp" line="1025"/>
        <source>Invalid realm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../client/jabber-client.cpp" line="1027"/>
        <source>SASL mechanism too weak for this account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../client/jabber-client.cpp" line="1029"/>
        <source>Not authorized</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../client/jabber-client.cpp" line="1031"/>
        <source>Temporary auth failure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../client/jabber-client.cpp" line="1033"/>
        <source>Authentication error: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../client/jabber-client.cpp" line="1036"/>
        <source>Broken security layer (SASL)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../client/jabber-client.cpp" line="1038"/>
        <source>None</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>XMPP::Stanza::Error::Private</name>
    <message>
        <location filename="../libiris/src/xmpp/xmpp-core/xmpp_stanza.cpp" line="249"/>
        <source>Bad request</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/src/xmpp/xmpp-core/xmpp_stanza.cpp" line="249"/>
        <source>The sender has sent XML that is malformed or that cannot be processed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/src/xmpp/xmpp-core/xmpp_stanza.cpp" line="250"/>
        <source>Conflict</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/src/xmpp/xmpp-core/xmpp_stanza.cpp" line="250"/>
        <source>Access cannot be granted because an existing resource or session exists with the same name or address.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/src/xmpp/xmpp-core/xmpp_stanza.cpp" line="251"/>
        <source>Feature not implemented</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/src/xmpp/xmpp-core/xmpp_stanza.cpp" line="251"/>
        <source>The feature requested is not implemented by the recipient or server and therefore cannot be processed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/src/xmpp/xmpp-core/xmpp_stanza.cpp" line="252"/>
        <source>Forbidden</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/src/xmpp/xmpp-core/xmpp_stanza.cpp" line="252"/>
        <source>The requesting entity does not possess the required permissions to perform the action.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/src/xmpp/xmpp-core/xmpp_stanza.cpp" line="253"/>
        <source>Gone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/src/xmpp/xmpp-core/xmpp_stanza.cpp" line="253"/>
        <source>The recipient or server can no longer be contacted at this address.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/src/xmpp/xmpp-core/xmpp_stanza.cpp" line="254"/>
        <source>Internal server error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/src/xmpp/xmpp-core/xmpp_stanza.cpp" line="254"/>
        <source>The server could not process the stanza because of a misconfiguration or an otherwise-undefined internal server error.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/src/xmpp/xmpp-core/xmpp_stanza.cpp" line="255"/>
        <source>Item not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/src/xmpp/xmpp-core/xmpp_stanza.cpp" line="255"/>
        <source>The addressed JID or item requested cannot be found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/src/xmpp/xmpp-core/xmpp_stanza.cpp" line="256"/>
        <source>JID malformed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/src/xmpp/xmpp-core/xmpp_stanza.cpp" line="256"/>
        <source>The sending entity has provided or communicated an XMPP address (e.g., a value of the &apos;to&apos; attribute) or aspect thereof (e.g., a resource identifier) that does not adhere to the syntax defined in Addressing Scheme.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/src/xmpp/xmpp-core/xmpp_stanza.cpp" line="257"/>
        <source>Not acceptable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/src/xmpp/xmpp-core/xmpp_stanza.cpp" line="257"/>
        <source>The recipient or server understands the request but is refusing to process it because it does not meet criteria defined by the recipient or server (e.g., a local policy regarding acceptable words in messages).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/src/xmpp/xmpp-core/xmpp_stanza.cpp" line="258"/>
        <source>Not allowed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/src/xmpp/xmpp-core/xmpp_stanza.cpp" line="258"/>
        <source>The recipient or server does not allow any entity to perform the action.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/src/xmpp/xmpp-core/xmpp_stanza.cpp" line="259"/>
        <source>Not authorized</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/src/xmpp/xmpp-core/xmpp_stanza.cpp" line="259"/>
        <source>The sender must provide proper credentials before being allowed to perform the action, or has provided improper credentials.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/src/xmpp/xmpp-core/xmpp_stanza.cpp" line="260"/>
        <source>Payment required</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/src/xmpp/xmpp-core/xmpp_stanza.cpp" line="260"/>
        <source>The requesting entity is not authorized to access the requested service because payment is required.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/src/xmpp/xmpp-core/xmpp_stanza.cpp" line="261"/>
        <source>Recipient unavailable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/src/xmpp/xmpp-core/xmpp_stanza.cpp" line="261"/>
        <source>The intended recipient is temporarily unavailable.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/src/xmpp/xmpp-core/xmpp_stanza.cpp" line="262"/>
        <source>Redirect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/src/xmpp/xmpp-core/xmpp_stanza.cpp" line="262"/>
        <source>The recipient or server is redirecting requests for this information to another entity, usually temporarily.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/src/xmpp/xmpp-core/xmpp_stanza.cpp" line="263"/>
        <source>Registration required</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/src/xmpp/xmpp-core/xmpp_stanza.cpp" line="263"/>
        <source>The requesting entity is not authorized to access the requested service because registration is required.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/src/xmpp/xmpp-core/xmpp_stanza.cpp" line="264"/>
        <source>Remote server not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/src/xmpp/xmpp-core/xmpp_stanza.cpp" line="264"/>
        <source>A remote server or service specified as part or all of the JID of the intended recipient does not exist.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/src/xmpp/xmpp-core/xmpp_stanza.cpp" line="265"/>
        <source>Remote server timeout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/src/xmpp/xmpp-core/xmpp_stanza.cpp" line="265"/>
        <source>A remote server or service specified as part or all of the JID of the intended recipient (or required to fulfill a request) could not be contacted within a reasonable amount of time.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/src/xmpp/xmpp-core/xmpp_stanza.cpp" line="266"/>
        <source>Resource constraint</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/src/xmpp/xmpp-core/xmpp_stanza.cpp" line="266"/>
        <source>The server or recipient lacks the system resources necessary to service the request.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/src/xmpp/xmpp-core/xmpp_stanza.cpp" line="267"/>
        <source>Service unavailable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/src/xmpp/xmpp-core/xmpp_stanza.cpp" line="267"/>
        <source>The server or recipient does not currently provide the requested service.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/src/xmpp/xmpp-core/xmpp_stanza.cpp" line="268"/>
        <source>Subscription required</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/src/xmpp/xmpp-core/xmpp_stanza.cpp" line="268"/>
        <source>The requesting entity is not authorized to access the requested service because a subscription is required.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/src/xmpp/xmpp-core/xmpp_stanza.cpp" line="269"/>
        <source>Undefined condition</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/src/xmpp/xmpp-core/xmpp_stanza.cpp" line="269"/>
        <source>The error condition is not one of those defined by the other conditions in this list.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/src/xmpp/xmpp-core/xmpp_stanza.cpp" line="270"/>
        <source>Unexpected request</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libiris/src/xmpp/xmpp-core/xmpp_stanza.cpp" line="270"/>
        <source>The recipient or server understood the request but was not expecting it at this time (e.g., the request was out of order).</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>XMPP::Task</name>
    <message>
        <location filename="../libiris/src/xmpp/xmpp-im/xmpp_task.cpp" line="165"/>
        <source>Disconnected</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>XmlConsole</name>
    <message>
        <location filename="../gui/windows/xml-console.cpp" line="33"/>
        <source>XML Console - %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
