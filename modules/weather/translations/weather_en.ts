<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="en_US">
<context>
    <name>@default</name>
    <message>
        <location filename="../weather.cpp" line="71"/>
        <source>New forecast has been fetched</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AutoDownloader</name>
    <message>
        <location filename="../autodownloader.cpp" line="160"/>
        <source>New forecast has been fetched</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EnterCityDialog</name>
    <message>
        <location filename="../getcitydialog.cpp" line="37"/>
        <source>City search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../getcitydialog.cpp" line="42"/>
        <source>City:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../getcitydialog.cpp" line="51"/>
        <source>Find</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../getcitydialog.cpp" line="67"/>
        <source>Enter city name!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SearchAndShowForecastFrame</name>
    <message>
        <location filename="../showforecastframe.cpp" line="298"/>
        <source>Searching for &lt;b&gt;%1&lt;/b&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../showforecastframe.cpp" line="313"/>
        <source>&lt;b&gt;%1&lt;/b&gt; not found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../showforecastframe.cpp" line="324"/>
        <source>Cannot load page %1.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SearchingCityDialog</name>
    <message>
        <location filename="../getcitydialog.cpp" line="88"/>
        <source>City search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../getcitydialog.cpp" line="96"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../getcitydialog.cpp" line="121"/>
        <source>Retrieving city from public directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../getcitydialog.cpp" line="178"/>
        <source>Searching for %1 in %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../getcitydialog.cpp" line="191"/>
        <source>City not found!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SelectCityDialog</name>
    <message>
        <location filename="../getcitydialog.cpp" line="237"/>
        <source>City search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../getcitydialog.cpp" line="239"/>
        <source>Select city:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../getcitydialog.cpp" line="245"/>
        <source>New search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../getcitydialog.cpp" line="246"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ShowForecastDialog</name>
    <message>
        <location filename="../show_forecast_dialog.cpp" line="48"/>
        <source>%1 - Forecast</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ShowForecastFrameBase</name>
    <message>
        <location filename="../showforecastframe.cpp" line="37"/>
        <source>Pressure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../showforecastframe.cpp" line="38"/>
        <source>Rain</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../showforecastframe.cpp" line="39"/>
        <source>Snow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../showforecastframe.cpp" line="40"/>
        <source>Wind speed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../showforecastframe.cpp" line="41"/>
        <source>Description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../showforecastframe.cpp" line="42"/>
        <source>Humidity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../showforecastframe.cpp" line="43"/>
        <source>Dew point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../showforecastframe.cpp" line="44"/>
        <source>Visibility</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../showforecastframe.cpp" line="115"/>
        <source>Copy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../showforecastframe.cpp" line="116"/>
        <source>Go to Web page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../showforecastframe.cpp" line="117"/>
        <source>Change city...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../showforecastframe.cpp" line="126"/>
        <source>Forecast download</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../showforecastframe.cpp" line="143"/>
        <source>Cannot load page %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../showforecastframe.cpp" line="145"/>
        <source>Parse error page %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Weather</name>
    <message>
        <location filename="../weather.cpp" line="66"/>
        <source>&lt;u&gt;%l&lt;/u&gt; - &lt;b&gt;%d:&lt;/b&gt;&lt;br&gt;Temperature: %t&lt;br&gt;Pressure: %p</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../weather.cpp" line="67"/>
        <source>Temperature in %l: %t</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../weather.cpp" line="74"/>
        <source>Local forecast</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../weather.cpp" line="76"/>
        <source>Forecast for...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../weather.cpp" line="78"/>
        <source>Show contact weather</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WeatherCfgUiHandler</name>
    <message>
        <location filename="../weather_cfg_ui_handler.cpp" line="40"/>
        <source>Servers priorites:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../weather_cfg_ui_handler.cpp" line="45"/>
        <source>Server name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../weather_cfg_ui_handler.cpp" line="55"/>
        <source>Up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../weather_cfg_ui_handler.cpp" line="56"/>
        <source>Down</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
