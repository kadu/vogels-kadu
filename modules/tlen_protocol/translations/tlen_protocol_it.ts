<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="it">
<context>
    <name>@default</name>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="4"/>
        <source>Buddies</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="7"/>
        <source>General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="10"/>
        <source>Protocol</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="13"/>
        <source>Tlen ID</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TlenAddAccountWidget</name>
    <message>
        <location filename="../gui/widgets/tlen-add-account-widget.cpp" line="69"/>
        <source>Tlen.pl login</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/tlen-add-account-widget.cpp" line="74"/>
        <source>Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/tlen-add-account-widget.cpp" line="76"/>
        <source>Remember password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/tlen-add-account-widget.cpp" line="87"/>
        <source>Account identity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/tlen-add-account-widget.cpp" line="89"/>
        <source>&lt;font size=&apos;-1&apos;&gt;&lt;i&gt;Select or enter the identity that will be associated with this account.&lt;/i&gt;&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/tlen-add-account-widget.cpp" line="99"/>
        <source>Add Account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/tlen-add-account-widget.cpp" line="100"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TlenCreateAccountWidget</name>
    <message>
        <location filename="../gui/widgets/tlen-create-account-widget.cpp" line="58"/>
        <source>Account name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/tlen-create-account-widget.cpp" line="69"/>
        <source>Tlen.pl login</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/tlen-create-account-widget.cpp" line="75"/>
        <source>Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/tlen-create-account-widget.cpp" line="86"/>
        <source>Account description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/tlen-create-account-widget.cpp" line="92"/>
        <source>Remember password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/tlen-create-account-widget.cpp" line="96"/>
        <source>Add this account</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TlenEditAccountWidget</name>
    <message>
        <location filename="../gui/widgets/tlen-edit-account-widget.cpp" line="73"/>
        <source>Apply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/tlen-edit-account-widget.cpp" line="76"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/tlen-edit-account-widget.cpp" line="79"/>
        <source>Delete account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/tlen-edit-account-widget.cpp" line="103"/>
        <source>Connect at start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/tlen-edit-account-widget.cpp" line="106"/>
        <source>Account name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/tlen-edit-account-widget.cpp" line="112"/>
        <source>Tlen.pl Login</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/tlen-edit-account-widget.cpp" line="118"/>
        <source>Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/tlen-edit-account-widget.cpp" line="125"/>
        <source>Remember password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/tlen-edit-account-widget.cpp" line="129"/>
        <source>Account description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/tlen-edit-account-widget.cpp" line="140"/>
        <source>Your photo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/tlen-edit-account-widget.cpp" line="147"/>
        <source>General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/tlen-edit-account-widget.cpp" line="153"/>
        <source>Personal Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/tlen-edit-account-widget.cpp" line="159"/>
        <source>Buddies</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/tlen-edit-account-widget.cpp" line="165"/>
        <source>Connection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/tlen-edit-account-widget.cpp" line="169"/>
        <source>Proxy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/tlen-edit-account-widget.cpp" line="212"/>
        <source>Confirm account removal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/tlen-edit-account-widget.cpp" line="213"/>
        <source>Are you sure do you want to remove account %1 (%2)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/tlen-edit-account-widget.cpp" line="217"/>
        <source>Remove Account</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TlenPersonalInfoWidget</name>
    <message>
        <location filename="../gui/widgets/tlen-personal-info-widget.cpp" line="62"/>
        <source>Nick</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/tlen-personal-info-widget.cpp" line="63"/>
        <source>First name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/tlen-personal-info-widget.cpp" line="64"/>
        <source>Last name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/tlen-personal-info-widget.cpp" line="65"/>
        <source>Sex</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/tlen-personal-info-widget.cpp" line="66"/>
        <source>Birth year</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/tlen-personal-info-widget.cpp" line="67"/>
        <source>City</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/tlen-personal-info-widget.cpp" line="68"/>
        <source>Looking for</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/tlen-personal-info-widget.cpp" line="69"/>
        <source>Job</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/tlen-personal-info-widget.cpp" line="70"/>
        <source>Today plans</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/tlen-personal-info-widget.cpp" line="71"/>
        <source>Show status</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/tlen-personal-info-widget.cpp" line="72"/>
        <source>Have mic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/tlen-personal-info-widget.cpp" line="73"/>
        <source>Have cam</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/tlen-personal-info-widget.cpp" line="74"/>
        <source>e-mail</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/tlen-personal-info-widget.cpp" line="80"/>
        <source>Unknown gender</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/tlen-personal-info-widget.cpp" line="81"/>
        <source>Male</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/tlen-personal-info-widget.cpp" line="82"/>
        <source>Female</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/tlen-personal-info-widget.cpp" line="87"/>
        <location filename="../gui/widgets/tlen-personal-info-widget.cpp" line="93"/>
        <location filename="../gui/widgets/tlen-personal-info-widget.cpp" line="99"/>
        <source>Unknown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/tlen-personal-info-widget.cpp" line="88"/>
        <location filename="../gui/widgets/tlen-personal-info-widget.cpp" line="94"/>
        <location filename="../gui/widgets/tlen-personal-info-widget.cpp" line="100"/>
        <source>1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/tlen-personal-info-widget.cpp" line="89"/>
        <location filename="../gui/widgets/tlen-personal-info-widget.cpp" line="95"/>
        <location filename="../gui/widgets/tlen-personal-info-widget.cpp" line="101"/>
        <source>2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/tlen-personal-info-widget.cpp" line="90"/>
        <location filename="../gui/widgets/tlen-personal-info-widget.cpp" line="96"/>
        <location filename="../gui/widgets/tlen-personal-info-widget.cpp" line="102"/>
        <source>3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/tlen-personal-info-widget.cpp" line="91"/>
        <location filename="../gui/widgets/tlen-personal-info-widget.cpp" line="97"/>
        <location filename="../gui/widgets/tlen-personal-info-widget.cpp" line="103"/>
        <source>4</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TlenProtocol</name>
    <message>
        <location filename="../tlen-protocol.cpp" line="228"/>
        <source>Tlen ID not set!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../tlen-protocol.cpp" line="236"/>
        <source>Please provide password for %1 (%2) account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../tlen-protocol.cpp" line="651"/>
        <source>User authorization</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../tlen-protocol.cpp" line="652"/>
        <source>User %1 requested authorization from You.Do you want to authorize him?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../tlen-protocol.cpp" line="655"/>
        <source>&amp;Yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../tlen-protocol.cpp" line="655"/>
        <source>&amp;No</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TlenProtocolFactory</name>
    <message>
        <location filename="../tlen-protocol-factory.cpp" line="106"/>
        <source>Tlen ID:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>fileTransferThread</name>
    <message>
        <location filename="../QtTlen/filetransfer.cpp" line="196"/>
        <source>Choose files to send</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>rosterItem</name>
    <message>
        <location filename="../QtTlen/roster_item.cpp" line="183"/>
        <source>Contacts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../QtTlen/roster_item.cpp" line="237"/>
        <source>No authorization</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../QtTlen/roster_item.cpp" line="242"/>
        <source>Awaiting authorization</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../QtTlen/roster_item.cpp" line="247"/>
        <source>Unavailable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../QtTlen/roster_item.cpp" line="252"/>
        <source>Currently available</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>tlen</name>
    <message>
        <location filename="../QtTlen/tlen.cpp" line="278"/>
        <source>Contacts</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
